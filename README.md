# Klotures
A popular desktop icon organizer tool this is not.

![](https://gitlab.com/jkushmaul/klotures/raw/master/icon.png "Klotures")

# Purpose
Desktop icon organization for people who still like desktop icons.  At the expense of ease of installation, I've attempted to keep this as barebones as possible.  
There are no hidden services, shell extensions, installer scripts etc.

I didn't want to spend 5.99 for another popular app, so I spent months writing my own...  
Not very cost effective - but it was fun.  It's a curse.


1. If this is familiar  
![](https://gitlab.com/jkushmaul/klotures/raw/master/docs/unorganized_desktop.png)

1. And this  
![](https://gitlab.com/jkushmaul/klotures/raw/master/docs/manual_desktop_organization.png)

1. Then klotures may be for you  
![](https://gitlab.com/jkushmaul/klotures/raw/master/docs/organized_with_klotures.png )

1. A short demo video  
![](https://gitlab.com/jkushmaul/klotures/raw/master/docs/KloturesDemo.webm)

# Install

1. Download a zip file from: https://gitlab.com/jkushmaul/klotures/-/releases
2. Extract
3. Run Klotures.exe

Optionally

1. Create a shortcut in
`%APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup`

# Building
Project page: https://gitlab.com/jkushmaul/klotures

You can build in Visual Studio 2019, or see the commands in .gitlab-ci.yml for cli building.

# Features
1. Starts with one default desktop kloture*
1. Renamable klotures
1. Lock/Unlock of klotures  
1. Move/Resize klotures
1. Minimize klotures  
1. Hide klotures  
1. Find klotures

* Keeping desktop icons on the actual desktop was surprisingly difficult.  I cried and ran away, by tucking them away in a default.