﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klotures
{
    /// <summary>
    /// Interaction logic for EditBlock.xaml
    /// </summary>
    public partial class EditControl : UserControl
    {
        public class EditEventArgs
        {
            public string Text { get; protected set; }
            public EditEventArgs(string text)
            {
                this.Text = text;
            }
        }

        public event EventHandler<EditEventArgs> OnEditSave;
        public event EventHandler OnEditCancel;

        public string Text
        {
            get => ReadBlock.Text;
            set => ReadBlock.Text = value;
        }

        public EditControl()
        {
            InitializeComponent();
            EditBlock.FontSize *= 2;
            ReadBlock.FontSize *= 2;

            SetEditMode(false);
        }

        public void SetEditMode(bool isEdit)
        {
            if (isEdit)
            {
                ReadBlock.Visibility = Visibility.Collapsed;
                ReadBlock.IsEnabled = false;
                EditBlock.Text = ReadBlock.Text;
                EditBlock.IsEnabled = true;
                EditBlock.Visibility = Visibility.Visible;
                EditBlock.Focus();
                EditBlock.SelectAll();
            }
            else
            {
                EditBlock.Visibility = Visibility.Collapsed;
                EditBlock.IsEnabled = false;
                ReadBlock.IsEnabled = true;
                ReadBlock.Visibility = Visibility.Visible;
            }
        }

        private void CancelEdit()
        {
            EditBlock.Text = ReadBlock.Text;
            SetEditMode(false);
            if (OnEditCancel != null)
            {
                OnEditCancel(this, EventArgs.Empty);
            }
        }
        private void SaveEdit()
        {
            SetEditMode(false);
            if (OnEditSave != null)
            {
                OnEditSave(this, new EditEventArgs(EditBlock.Text));
            }
        }

        private void EditBlock_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            CancelEdit();
        }

        private void EditBlock_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                    CancelEdit();
                    break;
                case Key.Return:
                    SaveEdit();
                    break;
            }
        }
 
        private void EditBlock_LostFocus(object sender, RoutedEventArgs e)
        {
            CancelEdit();
        }
    }
}
