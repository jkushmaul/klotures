﻿using System;

namespace Klotures.lib.win32.flags
{
    public partial class NativeFlags
    {
        [Flags]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1712:Do not prefix enum values with type name", Justification = "<Pending>")]
        public enum ILD : uint
        {
            ILD_NORMAL = 0x00000000,
            ILD_TRANSPARENT = 0x00000001,
            ILD_MASK = 0x00000010,
            ILD_IMAGE = 0x00000020,
            ILD_ROP = 0x00000040,
            ILD_BLEND25 = 0x00000002,
            ILD_BLEND50 = 0x00000004,
            ILD_OVERLAYMASK = 0x00000F00,
            ILD_PRESERVEALPHA = 0x00001000,  // This preserves the alpha channel in dest
            ILD_SCALE = 0x00002000,  // Causes the image to be scaled to cx, cy instead of clipped
            ILD_DPISCALE = 0x00004000,
            ILD_SELECTED = ILD_BLEND50,
            ILD_FOCUS = ILD_BLEND25,
            ILD_BLEND = ILD_BLEND50
        }
    }
}
