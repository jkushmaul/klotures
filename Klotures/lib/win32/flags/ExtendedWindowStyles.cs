﻿using System;

namespace Klotures.lib.win32.flags
{
    public partial class NativeFlags
    {

        [Flags]
        internal enum ExtendedWindowStyles
        {
            // ...
            WS_EX_TOOLWINDOW = 0x00000080,
            WS_VISIBLE = 0x10000000
            // ...
        }
    }
}
