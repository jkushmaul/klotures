﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security;
using static Klotures.lib.win32.flags.NativeFlags;

namespace Klotures.lib.win32.structs
{
    public static class NativeStructs
    {
        #region Win API constants

        internal const int WM_TIMER = 0x0113;
        internal const int PS_SOLID = 0;

        internal const uint PROCESS_VM_OPERATION = 0x8;
        internal const uint PROCESS_VM_READ = 0x10;
        internal const uint PROCESS_VM_WRITE = 0x20;
        internal const uint MEM_COMMIT = 0x1000;
        internal const uint MEM_RELEASE = 0x8000;
        internal const uint MEM_RESERVE = 0x2000;
        internal const uint PAGE_READWRITE = 4;

        #endregion

        #region Win API enums

        internal enum RedrawWindowFlags
        {
            RDW_INVALIDATE = 0x0001,
            RDW_NOERASE = 0x0020,
            RDW_ERASE = 0x0004,
            RDW_UPDATENOW = 0x0100
        }

        [Flags]
        internal enum SendMessageTimeoutFlags : uint
        {
            SMTO_NORMAL = 0x0,
            SMTO_BLOCK = 0x1,
            SMTO_ABORTIFHUNG = 0x2,
            SMTO_NOTIMEOUTIFNOTHUNG = 0x8,
            SMTO_ERRORONEXIT = 0x20
        }

        #endregion

        #region Structs

        [ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown), Guid("6d5140c1-7436-11ce-8034-00aa006009fa"), SuppressUnmanagedCodeSecurity]
        public interface _IServiceProvider
        {
            void QueryService(
                    [In, MarshalAs(UnmanagedType.LPStruct)] Guid guid,
                    [In, MarshalAs(UnmanagedType.LPStruct)] Guid riid,
                    [MarshalAs(UnmanagedType.Interface)] out object Obj);
        }

        [StructLayoutAttribute(LayoutKind.Sequential)]
        public struct WINDOWINFO
        {
            public uint cbSize;
            public RECT rcWindow;
            public RECT rcClient;
            public uint dwStyle;
            public uint dwExStyle;
            public uint dwWindowStatus;
            public uint cxWindowBorders;
            public uint cyWindowBorders;
            public ushort atomWindowType;
            public ushort wCreatorVersion;

            public WINDOWINFO(Boolean? filler)
                : this()   // Allows automatic initialization of "cbSize" with "new WINDOWINFO(null/true/false)".
            {
                cbSize = (UInt32)(Marshal.SizeOf(typeof(WINDOWINFO)));
            }

        }

        [StructLayoutAttribute(LayoutKind.Sequential)]
        public struct LV_ITEM
        {
            public LVIF mask;
            public int iItem;
            public int iSubItem;
            public int state;
            public LVIF stateMask;
            public IntPtr pszText; // string
            public int cchTextMax;
            public int iImage;
            public IntPtr lParam;
            public int iIndent;
            public int iGroupId;
            public int cColumns;
            public IntPtr puColumns;
        }

        [StructLayoutAttribute(LayoutKind.Sequential)]
        public struct LVGROUP
        {
            public uint cbSize;
            public LVGF mask;
            public IntPtr pszHeader;
            public int cchHeader;
            public IntPtr pszFooter;
            public int cchFooter;
            public int iGroupId;
            public LVGS stateMask;
            public LVGS state;
            public uint uAlign;
            public IntPtr pszSubtitle;
            public uint cchSubtitle;
            public IntPtr pszTask;
            public uint cchTask;
            public IntPtr pszDescriptionTop;
            public uint cchDescriptionTop;
            public IntPtr pszDescriptionBottom;
            public uint cchDescriptionBottom;
            public int iTitleImage;
            public int iExtendedImage;
            public int iFirstItem;
            public uint cItems;
            public IntPtr pszSubsetTitle;
            public uint cchSubsetTitle;
        }

        [StructLayoutAttribute(LayoutKind.Sequential)]
        public struct LVCOLUMN
        {
            public uint mask;
            public int fmt;
            public int cx;
            public IntPtr pszText;
            public int cchTextMax;
            public int iSubItem;
            public int iImage;
            public int iOrder;
            public int cxMin;
            public int cxDefault;
            public int cxIdeal;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct POINT
        {
            int x;
            int y;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct IMAGELISTDRAWPARAMS
        {
            internal int cbSize;
            internal IntPtr himl;
            internal int i;
            internal IntPtr hdcDst;
            internal int x;
            internal int y;
            internal int cx;
            internal int cy;
            internal int xBitmap;    // x offest from the upperleft of bitmap
            internal int yBitmap;    // y offset from the upperleft of bitmap
            internal int rgbBk;
            internal int rgbFg;
            internal int fStyle;
            internal int dwRop;
            internal int fState;
            internal int Frame;
            internal int crEffect;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct IMAGEINFO
        {
            internal IntPtr hbmImage;
            internal IntPtr hbmMask;
            internal int Unused1;
            internal int Unused2;
            internal RECT rcImage;
        }
        internal struct SHFILEINFO
        {
            public SHFILEINFO(bool b)
            {
                hIcon = IntPtr.Zero; iIcon = 0; dwAttributes = 0; szDisplayName = ""; szTypeName = "";
            }

            // Handle to the icon representing the file

            internal IntPtr hIcon;

            // Index of the icon within the image list

            internal int iIcon;

            // Various attributes of the file

            internal uint dwAttributes;

            // Path to the file

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]

            internal string szDisplayName;

            // File type

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)]

            internal string szTypeName;

        };

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            internal int left;
            internal int top;
            internal int right;
            internal int bottom;
            internal RECT(int left, int top, int right, int bottom)
            {
                this.left = left;
                this.top = top;
                this.right = right;
                this.bottom = bottom;
            }
        }

        #endregion

        [StructLayout(LayoutKind.Sequential)]
        public struct ShellNotifyStruct
        {
            internal IntPtr item1;
            internal IntPtr item2;
        };

        [StructLayout(LayoutKind.Explicit)]
        public struct STRRET
        {
            [FieldOffset(0)]
            public UInt32 uType;                        // One of the STRRET_* values

            [FieldOffset(4)]
            public IntPtr pOleStr;                      // must be freed by caller of GetDisplayNameOf

            [FieldOffset(4)]
            public IntPtr pStr;                         // NOT USED

            [FieldOffset(4)]
            public UInt32 uOffset;                      // Offset into SHITEMID

            [FieldOffset(4)]
            public IntPtr cStr;                         // Buffer to fill in (ANSI)
        }


        /// <summary>Provides a handle to a Windows image list.</summary>
        [StructLayout(LayoutKind.Sequential), DebuggerDisplay("{handle}")]
        public struct HIMAGELIST
        {
            private IntPtr handle;

            /// <summary>Initializes a new instance of the <see cref="HIMAGELIST"/> struct.</summary>
            /// <param name="preexistingHandle">An <see cref="IntPtr"/> object that represents the pre-existing handle to use.</param>
            public HIMAGELIST(IntPtr preexistingHandle) => handle = preexistingHandle;

            /// <summary>Returns an invalid handle by instantiating a <see cref="HIMAGELIST"/> object with <see cref="IntPtr.Zero"/>.</summary>
            public static HIMAGELIST NULL => new HIMAGELIST(IntPtr.Zero);

            /// <summary>Gets a value indicating whether this instance is a null handle.</summary>
            public bool IsNull => handle == IntPtr.Zero;

            /// <summary>Performs an explicit conversion from <see cref="HIMAGELIST"/> to <see cref="IntPtr"/>.</summary>
            /// <param name="h">The handle.</param>
            /// <returns>The result of the conversion.</returns>
            public static explicit operator IntPtr(HIMAGELIST h) => h.handle;

            /// <summary>Performs an implicit conversion from <see cref="IntPtr"/> to <see cref="HIMAGELIST"/>.</summary>
            /// <param name="h">The pointer to a handle.</param>
            /// <returns>The result of the conversion.</returns>
            public static implicit operator HIMAGELIST(IntPtr h) => new HIMAGELIST(h);

            /// <summary>Implements the operator !=.</summary>
            /// <param name="h1">The first handle.</param>
            /// <param name="h2">The second handle.</param>
            /// <returns>The result of the operator.</returns>
            public static bool operator !=(HIMAGELIST h1, HIMAGELIST h2) => !(h1 == h2);

            /// <summary>Implements the operator ==.</summary>
            /// <param name="h1">The first handle.</param>
            /// <param name="h2">The second handle.</param>
            /// <returns>The result of the operator.</returns>
            public static bool operator ==(HIMAGELIST h1, HIMAGELIST h2) => h1.Equals(h2);

            /// <inheritdoc/>
            public override bool Equals(object obj) => obj is HIMAGELIST h ? handle == h.handle : false;

            /// <inheritdoc/>
            public override int GetHashCode() => handle.GetHashCode();

            /// <inheritdoc/>
            public IntPtr DangerousGetHandle() => handle;
        }

        //https://pinvoke.net/default.aspx/Structures/SHChangeNotifyEntry.html
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public struct SHChangeNotifyEntry
        {
            public IntPtr pIdl;
            [MarshalAs(UnmanagedType.Bool)] public Boolean Recursively;
        }
    }
}
