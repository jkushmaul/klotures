﻿using Klotures.lib.win32.structs;
using Klotures.win32;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using static Klotures.win32.NativeMethods;

namespace Klotures.lib.jkushmaul
{
    /// <summary>
    /// Straight ripped off from stack overflow, like this one
    /// https://stackoverflow.com/questions/47694148/size-of-bitmap-in-drawimage
    /// Clearly repeated work
    /// </summary>
    public static class ImageTools
    {



        internal static ImageSource ToImageSource(this Icon icon)
        {
            Bitmap bitmap = icon.ToBitmap();
            IntPtr hBitmap = bitmap.GetHbitmap();
            try
            {
                ImageSource wpfBitmap = Imaging.CreateBitmapSourceFromHBitmap(
                hBitmap,
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());

                return wpfBitmap;
            } finally
            {
                NativeMethods.DeleteObject(hBitmap);
            }
            

            
        }




    }
}
