﻿using SharpShell.Interop;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
/*
The MIT License (MIT)
Copyright (c) 2014 Dave Kerr
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This file borrowed from SharpShell source
 */
namespace Klotures.lib.SharpShellHidden
{
    /// <summary>
    /// The Shell Image List.
    /// 
    /// I had to extract these from SharpShell to be able to use them directly.
    /// </summary>
    public static class ShellImageList
    {
        /// <summary>
        /// Initializes the <see cref="ShellImageList"/> class.
        /// </summary>
        static ShellImageList()
        {
        }

        /// <summary>
        /// Gets the image list interface.
        /// </summary>
        /// <param name="imageListSize">Size of the image list.</param>
        /// <returns>The IImageList for the shell image list of the given size.</returns>
        public static IntPtr GetImageList(ShellImageListSize imageListSize)
        {
            //  Do we have the image list?
            IImageList imageList;
            if (imageLists.TryGetValue(imageListSize, out imageList))
                return GetImageListHandle(imageList);

            //  We don't have the image list, create it.
            SharpShell.Interop.Shell32.SHGetImageList((int)imageListSize, ref SharpShell.Interop.Shell32.IID_IImageList, ref imageList);

            //  Add it to the dictionary.
            imageLists.Add(imageListSize, imageList);

            //  Return it.
            return GetImageListHandle(imageList);
        }

        /// <summary>
        /// Gets the image list handle.
        /// </summary>
        /// <param name="imageList">The image list.</param>
        /// <returns>The image list handle for the image list.</returns>
        private static IntPtr GetImageListHandle(IImageList imageList)
        {
            return Marshal.GetIUnknownForObject(imageList);
        }

        /// <summary>
        /// The shell image lists.
        /// </summary>
        private readonly static Dictionary<ShellImageListSize, IImageList> imageLists = new Dictionary<ShellImageListSize, IImageList>();
    }

    /// <summary>
    /// Shell Image List sizes. These correspond exactly by value to the sizes such 
    /// as SHIL_LARGE, SHIL_JUMBO, etc.
    /// </summary>
    public enum ShellImageListSize
    {
        /// <summary>
        /// The image size is normally 32x32 pixels. However, if the Use large icons option is selected from the Effects section of the Appearance tab in Display Properties, the image is 48x48 pixels.
        /// </summary>
        Large = 0x0,

        /// <summary>
        /// These images are the Shell standard small icon size of 16x16, but the size can be customized by the user.
        /// </summary>
        Small = 0x1,

        /// <summary>
        /// These images are the Shell standard extra-large icon size. This is typically 48x48, but the size can be customized by the user.
        /// </summary>
        ExtraLarge = 0x2,

        /// <summary>
        /// These images are the size specified by GetSystemMetrics called with SM_CXSMICON and GetSystemMetrics called with SM_CYSMICON.
        /// </summary>
        SysSmall = 0x3,

        /// <summary>
        /// Windows Vista and later. The image is normally 256x256 pixels.
        /// </summary>
        Jumbo = 0x4
    }

}
