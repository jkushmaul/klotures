﻿using Klotures.win32;
using SharpShell.Interop;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;


/*
The MIT License (MIT)
Copyright (c) 2014 Dave Kerr
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This file borrowed from SharpShell source
 */
namespace Klotures.lib.SharpShellHidden
{
    /// <summary>
    /// Represents a ShellItem object.
    /// 
    /// I had to extract these from SharpShell to be able to use them directly.
    /// </summary>
    public class ShellItem : IDisposable
    {
        /// <summary>
        /// The Child Type flags.
        /// </summary>
        [Flags]
        public enum ChildTypes
        {
            Folders = 1,
            Files = 2,
            Hidden = 4
        }

        /// <summary>
        /// Initializes the <see cref="ShellItem"/> class.
        /// </summary>
        static ShellItem()
        {
            //  Create the lazy desktop shell folder.
            desktopShellFolder = new Lazy<ShellItem>(CreateDesktopShellFolder);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShellItem"/> class.
        /// </summary>
        public ShellItem()
        {
            //  Create the lazy path.
            path = new Lazy<string>(CreatePath);
            overlayIcon = new Lazy<Icon>(CreateOverlayIcon);
        }

 

        /// <summary>
        /// Creates the icon.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        private Icon CreateOverlayIcon( )
        {
            SHFILEINFO shinfo = new SHFILEINFO();
            uint cbFileInfo = (uint)Marshal.SizeOf(shinfo);

            IntPtr result = SharpShell.Interop.Shell32.SHGetFileInfo(PIDL, 0, out shinfo, (uint)Marshal.SizeOf(shinfo),
                     SHGFI.SHGFI_PIDL | SHGFI.SHGFI_SYSICONINDEX | SHGFI.SHGFI_ICON | SHGFI.SHGFI_SHELLICONSIZE | SHGFI.SHGFI_ADDOVERLAYS);
            if (result == IntPtr.Zero)
            {
                return null;
            }
            const int SHIL_EXTRALARGE = 0x2;
            var iconIndex = shinfo.iIcon;
            Guid iidImageList = new Guid("46EB5926-582E-4017-9FDF-E8998DAA0950");

            NativeMethods.IImageList iml;
            int size = SHIL_EXTRALARGE;
            var hres = NativeMethods.SHGetImageList(size, ref iidImageList, out iml); // writes iml

            IntPtr hIcon = IntPtr.Zero;
            int ILD_TRANSPARENT = 1;
            hres = iml.GetIcon(iconIndex, ILD_TRANSPARENT, ref hIcon);


            return System.Drawing.Icon.FromHandle(hIcon);
        }

        /// <summary>
        /// Creates the desktop shell folder.
        /// </summary>
        /// <returns>The desktop shell folder.</returns>
        private static ShellItem CreateDesktopShellFolder()
        {
            //  Get the desktop shell folder interface. 
            IShellFolder desktopShellFolderInterface = null;
            var result = SharpShell.Interop.Shell32.SHGetDesktopFolder(out desktopShellFolderInterface);

            //  Validate the result.
            if (result != 0)
            {
                //  Throw the failure as an exception.
                Marshal.ThrowExceptionForHR(result);
            }

            //  Get the dekstop PDIL.
            var desktopPIDL = IntPtr.Zero;
            result = SharpShell.Interop.Shell32.SHGetFolderLocation(IntPtr.Zero, CSIDL.CSIDL_DESKTOP, IntPtr.Zero, 0, out desktopPIDL);

            //  Validate the result.
            if (result != 0)
            {
                //  Throw the failure as an exception.
                Marshal.ThrowExceptionForHR(result);
            }

            //  Get the file info.
            var fileInfo = new SHFILEINFO();
            SharpShell.Interop.Shell32.SHGetFileInfo(desktopPIDL, 0, out fileInfo, (uint)Marshal.SizeOf(fileInfo),
                SHGFI.SHGFI_DISPLAYNAME | SHGFI.SHGFI_PIDL | SHGFI.SHGFI_SMALLICON | SHGFI.SHGFI_SYSICONINDEX);

            //  Return the Shell Folder.
            return new ShellItem
            {
                DisplayName = fileInfo.szDisplayName,
                IconIndex = fileInfo.iIcon,
                HasSubFolders = true,
                IsFolder = true,
                ShellFolderInterface = desktopShellFolderInterface,
                PIDL = desktopPIDL
            };
        }

        /// <summary>
        /// Initialises the ShellItem, from its PIDL and parent.
        /// </summary>
        /// <param name="pidl">The pidl.</param>
        /// <param name="parentFolder">The parent folder.</param>
        /// 
        internal void Initialise(IntPtr pidl, ShellItem parentFolder)
        {
            Initialise(pidl, parentFolder, true);
        }
        internal void Initialise(IntPtr pidl, ShellItem parentFolder, bool relativePidl)
        {
            //  Set the parent item and relative pidl.
            ParentItem = parentFolder;
            if (relativePidl)
            { 
                //  Create the fully qualified PIDL.
                PIDL = SharpShell.Interop.Shell32.ILCombine(parentFolder.PIDL, pidl);
            } else
            {
                PIDL = pidl; 
            }


            //  Use the desktop folder to get attributes.
            var flags = SFGAO.SFGAO_FOLDER | SFGAO.SFGAO_HASSUBFOLDER | SFGAO.SFGAO_BROWSABLE | SFGAO.SFGAO_FILESYSTEM;
            //todo was this parentFolder.ShellFolderInterface.GetAttributesOf(1, ref pidl, ref flags);

            var apidl = Marshal.AllocCoTaskMem(IntPtr.Size * 1);
            Marshal.Copy(new IntPtr[] { pidl }, 0, apidl, 1);

            parentFolder.ShellFolderInterface.GetAttributesOf(1, apidl, ref flags);

            IsFolder = (flags & SFGAO.SFGAO_FOLDER) != 0;
            HasSubFolders = (flags & SFGAO.SFGAO_HASSUBFOLDER) != 0;

            //  Get the file info.
            var fileInfo = new SHFILEINFO();
            SharpShell.Interop.Shell32.SHGetFileInfo(PIDL, 0, out fileInfo, (uint)Marshal.SizeOf(fileInfo),
                SHGFI.SHGFI_SMALLICON | SHGFI.SHGFI_SYSICONINDEX | SHGFI.SHGFI_PIDL | SHGFI.SHGFI_DISPLAYNAME | SHGFI.SHGFI_TYPENAME | SHGFI.SHGFI_ATTRIBUTES);
             
            
            

            //  Set extended attributes.
            DisplayName = fileInfo.szDisplayName;
            Attributes = (SFGAO)fileInfo.dwAttributes;
            TypeName = fileInfo.szTypeName;
            IconIndex = fileInfo.iIcon;



            //  Are we a folder?
            if (IsFolder)
            {
                //  Bind the shell folder interface.
                IShellFolder shellFolderInterface;
                IntPtr ppv = IntPtr.Zero;
                var result = parentFolder.ShellFolderInterface.BindToObject(pidl, IntPtr.Zero, ref SharpShell.Interop.Shell32.IID_IShellFolder,
                    out ppv);//out shellFolderInterface);
   
                //  Validate the result.
                if (result != 0)
                {
                    //  Throw the failure as an exception.
                    Marshal.ThrowExceptionForHR((int)result);
                }

                shellFolderInterface = ((IShellFolder)Marshal.GetObjectForIUnknown(ppv));
                ShellFolderInterface = shellFolderInterface;
            }
           
        }

        /// <summary>
        /// Gets the system path for this shell item.
        /// </summary>
        /// <returns>A path string.</returns>
        private string CreatePath()
        {
            var stringBuilder = new StringBuilder(256);
            if (Attributes.HasFlag(SFGAO.SFGAO_FILESYSTEM))
            {
                //this is always blank on Type=="System Folder", I assume because they are guid, not filesystem.
                SharpShell.Interop.Shell32.SHGetPathFromIDList(PIDL, stringBuilder);
            } else
            {
                stringBuilder.Append("Special/").Append(DisplayName);
            }
            
            return stringBuilder.ToString();
        }

        /// <summary>
        /// Gets the children.
        /// </summary>
        /// <param name="childTypes">The child types.</param>
        /// <returns>
        /// The children.
        /// </returns>
        public IEnumerable<ShellItem> GetChildren(ChildTypes childTypes)
        {
            //  We'll return a list of children.
            var children = new List<ShellItem>();

            //  Create the enum flags from the childtypes.
            SHCONTF enumFlags = 0;
            if (childTypes.HasFlag(ChildTypes.Folders))
                enumFlags |= SHCONTF.SHCONTF_FOLDERS;
            if (childTypes.HasFlag(ChildTypes.Files))
                enumFlags |= SHCONTF.SHCONTF_NONFOLDERS;
            if (childTypes.HasFlag(ChildTypes.Hidden))
                enumFlags |= SHCONTF.SHCONTF_INCLUDEHIDDEN;

            try
            {
                //  Create an enumerator for the children.
                IEnumIDList pEnum;
                var result = ShellFolderInterface.EnumObjects(IntPtr.Zero, enumFlags, out pEnum);

                //  Validate the result.
                if (result != 0)
                {
                    //  Throw the failure as an exception.
                    Marshal.ThrowExceptionForHR((int)result);
                }

                // TODO: This logic should go in the pidl manager.

                //  Enumerate the children, ten at a time.
                const int batchSize = 10;
                var pidlArray = Marshal.AllocCoTaskMem(IntPtr.Size * 10);
                uint itemsFetched;
                result = WinError.S_OK;
                do
                {
                    result = pEnum.Next(batchSize, pidlArray, out itemsFetched);

                    //  Get each pidl.
                    var pidls = new IntPtr[itemsFetched];
                    Marshal.Copy(pidlArray, pidls, 0, (int)itemsFetched);
                    foreach (var childPidl in pidls)
                    {
                        //  Create a new shell folder.
                        var childShellFolder = new ShellItem();

                        //  Initialize it.
                        try
                        {
                            childShellFolder.Initialise(childPidl, this);
                        }
                        catch (Exception exception)
                        {
                            throw new InvalidOperationException("Failed to initialise child.", exception);
                        }

                        //  Add the child.
                        children.Add(childShellFolder);

                        //  Free the PIDL, reset the result.
                        Marshal.FreeCoTaskMem(childPidl);
                    }
                } while (result == WinError.S_OK);

                Marshal.FreeCoTaskMem(pidlArray);

                //  Release the enumerator.
                if (Marshal.IsComObject(pEnum))
                    Marshal.ReleaseComObject(pEnum);
            }
            catch (Exception exception)
            {
                throw new InvalidOperationException("Failed to enumerate children.", exception);
            }

            //  Sort the children.
            var sortedChildren = children.Where(c => c.IsFolder).ToList();
            sortedChildren.AddRange(children.Where(c => !c.IsFolder));

            //  Return the children.
            return sortedChildren;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            //  Release the shell folder interface.
            if (ShellFolderInterface != null)
                Marshal.ReleaseComObject(ShellFolderInterface);

            //  Free the PIDL.
            if (PIDL != IntPtr.Zero)
                Marshal.FreeCoTaskMem(PIDL);

            //  Suppress finalization.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.IsNullOrEmpty(DisplayName) ? base.ToString() : DisplayName;
        }

        /// <summary>
        /// The lazy desktop shell folder.
        /// </summary>
        private static readonly Lazy<ShellItem> desktopShellFolder;

        /// <summary>
        /// The lazy path.
        /// </summary>
        private readonly Lazy<string> path;

        /// <summary>
        /// The overlay icon.
        /// </summary>
        private readonly Lazy<Icon> overlayIcon;

        /// <summary>
        /// Gets the parent item.
        /// </summary>
        public ShellItem ParentItem { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this instance is folder.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is folder; otherwise, <c>false</c>.
        /// </value>
        public bool IsFolder { get; private set; }

        /// <summary>
        /// Gets the display name.
        /// </summary>
        public string DisplayName { get; private set; }

        /// <summary>
        /// Gets the name of the type.
        /// </summary>
        /// <value>
        /// The name of the type.
        /// </value>
        public string TypeName { get; private set; }

        /// <summary>
        /// Gets the attributes.
        /// </summary>
        public SFGAO Attributes { get; private set; }

        /// <summary>
        /// Gets the index of the icon.
        /// </summary>
        /// <value>
        /// The index of the icon.
        /// </value>
        public int IconIndex { get; private set; }

        /// <summary>
        /// Gets the ShellFolder of the Desktop.
        /// </summary>
        public static ShellItem DesktopShellFolder { get { return desktopShellFolder.Value; } }

        /// <summary>
        /// Gets the shell folder interface.
        /// </summary>
        public IShellFolder ShellFolderInterface { get; private set; }
         
        /// <summary>
        /// Gets the Full PIDL.
        /// </summary>
        public IntPtr PIDL { get; private set; }

 

        /// <summary>
        /// Gets a value indicating whether this instance has children.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has children; otherwise, <c>false</c>.
        /// </value>
        public bool HasSubFolders { get; private set; }

        /// <summary>
        /// Gets the path.
        /// </summary>
        public string Path { get { return path.Value; } }

        /// <summary>
        /// Gets the overlay icon.
        /// </summary>
        /// <value>
        /// The overlay icon.
        /// </value>
        public Icon OverlayIcon { get { return overlayIcon.Value; } }
    }
}
