﻿using System;
using System.IO;

namespace Klotures.lib.jkushmaul
{
    internal class DesktopWatcher : IDisposable
    {
        private FileSystemWatcher fsw;
        public DesktopWatcher(FileSystemEventHandler changedHandler, RenamedEventHandler renameHandler)
        {
            fsw = new FileSystemWatcher();
            fsw.Path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            fsw.Created += changedHandler;
            fsw.Deleted += changedHandler;
            fsw.Renamed += renameHandler;

            fsw.EnableRaisingEvents = true;

        }
        public void Dispose()
        {
            fsw.Dispose();
            fsw = null;
        }
    }
}
