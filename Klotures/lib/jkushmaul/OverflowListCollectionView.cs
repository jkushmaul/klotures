﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Klotures.lib.jkushmaul
{
    public class OverflowListCollectionView<T, C> : ListCollectionView
        where C: IComparer<T>
    {
        //public event NotifyCollectionChangedEventHandler CollectionChanged;

        private Func<object, bool> filter;
        private SortedObservableCollection<T, C> sortedObservable;
        private int limit;
        public int Limit { 
            get => limit;
            set 
            {
                limit = value;
                Console.WriteLine("Setting Limit to " + limit);
                sortedObservable.Sort();
                Refresh();
                Console.WriteLine("Setting Limit to " + limit + " - Done");
            }
        }

        public event NotifyCollectionChangedEventHandler OverflowNotifier;

        public new Func<object, bool> Filter
        {
            get => filter;
            set
            {
                filter = value;
                base.Filter = (item) => LimitFilter(item);
            }
         
        }
        private IComparer customSort;
        public new IComparer CustomSort
        {
            get => customSort;
            set
            {
                customSort = value;
                sortedObservable.CustomSort = (C) customSort;
            }
        }

        public IEnumerable Overflow
        {
            get
            {

                List<object> all = ((IEnumerable<object>)SourceCollection)
                   .Where(x => filter(x))
                   .ToList();
                if (all.Count() > Limit)
                {
                    return all.GetRange(Limit, all.Count() - Limit);
                }
                return null;
            }
        }

        public OverflowListCollectionView(SortedObservableCollection<T, C> list) : base(list)
        {
            this.sortedObservable = list;
            Filter = (item) => true;
        }


        private bool LimitFilter(object item)
        {
           if (Limit == 0)
            {
                return true;
            }
            int index = ((IEnumerable<object>)SourceCollection)
                .Where(x => filter(x))
                .ToList()
                .IndexOf(item);

            bool result = index >= 0 && index < Limit;
            Console.WriteLine("LimitFilter: " + item + "=" + result);
            NotifyCollectionChangedEventArgs e = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset);
            OverflowNotifier(this, e);
            return result;
        }
    }
}
