﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Klotures.lib.jkushmaul.controls.resizable
{
    

    public class ResizeEventArgs
    {
        [Flags]
        public enum ResizeDirection
        {
            NONE = 0,
            N = 1,
            E = 2,
            S = 4,
            W = 8
        }

        Point MousePosition { get; }
        ResizeDirection direction { get; }

        public ResizeEventArgs(Point p, ResizeDirection dir)
        {
            MousePosition = p;
            direction = dir;
        }
    }
}
