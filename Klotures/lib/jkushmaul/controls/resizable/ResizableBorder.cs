﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static Klotures.lib.jkushmaul.controls.resizable.ResizeEventArgs;

namespace Klotures.lib.jkushmaul.controls.resizable
{
    /// <summary>
    /// Interaction logic for ResizableBorder.xaml
    /// </summary>
    public class ResizableBorder
    {
        public event EventHandler<ResizeEventArgs> OnResizeDragMove;
        private ResizeDirection resize_dir = ResizeDirection.NONE;
        private bool is_dragging = false;
        private Point last_drag_position = new Point(0, 0);
        private IResizable resizable;

        private bool _IsEnabled = true;
        public bool IsEnabled
        {
            get { return _IsEnabled; }
            set
            {
                _IsEnabled = value;
                ResizableBorder_MouseUp(null, null);
            }
        }

        public int ResizeCornerGrace
        {
            get; set;
        }
        
        
        public ResizableBorder(IResizable resizable)
        {
            ResizeCornerGrace = 10;
            this.resizable = resizable;
            resizable.ResizeBorder.PreviewMouseMove += ResizableBorder_PreviewMouseMove;
            resizable.ResizeBorder.MouseDown += ResizableBorder_MouseDown;
            resizable.ResizeBorder.MouseUp += ResizableBorder_MouseUp;
        }
       

        private ResizeDirection GetResizeDir(Point p )
        {
            Control n = resizable.ResizableControl;
            Rect r = new Rect(Canvas.GetLeft(n), Canvas.GetTop(n), n.Width, n.Height);
            Thickness t = resizable.ResizeBorder.BorderThickness;

            bool north = p.Y >= 0 && p.Y <= ResizeCornerGrace;
            bool east = p.X >= n.Width - ResizeCornerGrace;
            bool south = p.Y >= n.Height - ResizeCornerGrace;
            bool west = p.X >= 0 && p.X <= ResizeCornerGrace;

            ResizeDirection dir = ResizeDirection.NONE;
            if (north) 
            {
                dir |= ResizeDirection.N;
            }
            if (east)
            {
                dir |= ResizeDirection.E;
            }
            if (south)
            {
                dir |= ResizeDirection.S;
            }
            if (west)
            {
                dir |= ResizeDirection.W;
            }
            return dir;
        }

        private Cursor ResizeMouseCursor(Point p)
        {

            ResizeDirection dir = GetResizeDir(p);

            if (dir.HasFlag(ResizeDirection.N) && dir.HasFlag(ResizeDirection.W))
            {
                return Cursors.SizeNWSE;
            }
            else if (dir.HasFlag(ResizeDirection.S) && dir.HasFlag(ResizeDirection.E))
            {
                return Cursors.SizeNWSE;
            }
            else if (dir.HasFlag(ResizeDirection.N) && dir.HasFlag(ResizeDirection.E))
            {
                return Cursors.SizeNESW;
            }
            else if (dir.HasFlag(ResizeDirection.S) && dir.HasFlag(ResizeDirection.W))
            {
                return Cursors.SizeNESW;
            } else if (dir == ResizeDirection.E || dir == ResizeDirection.W)
            {
                return Cursors.SizeWE;
            } else if (dir == ResizeDirection.N || dir == ResizeDirection.S)
            {
                return Cursors.SizeNS;
            } else
            {
                return null;
            }
        }

        
        public void Fire_OnResizeDragMove(ResizeEventArgs e)
        {
            if (OnResizeDragMove != null)
            {
                OnResizeDragMove(this, e);
            }
        }

        public bool IsOverBorder(Point mouseRelativeToBorder)
        {
            Thickness t = resizable.ResizeBorder.BorderThickness;
            Control n = resizable.ResizableControl;
            
            Rect r = new Rect(Canvas.GetLeft(n), Canvas.GetTop(n), n.Width, n.Height);

            Point mouse = mouseRelativeToBorder;
            return (mouse.X >= 0 && mouse.X <= t.Left) 
                    || (mouse.X >= r.Width - t.Right && mouse.X  <= r.Width)
                    || (mouse.Y >= r.Height - t.Bottom  && mouse.Y <= r.Height)
                    || (mouse.Y >= 0 && mouse.Y <= t.Top);
        }
        public void ResizableBorder_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (!IsEnabled)
            {
                return;
            }
            Point newMousePos = Mouse.GetPosition(resizable.ResizeBorder);

            if (!is_dragging)
            {
                if (IsOverBorder(newMousePos))
                {
                    Mouse.OverrideCursor = ResizeMouseCursor(newMousePos);
                } else
                {
                    Mouse.OverrideCursor = null;
                }
                 
                return;
            } else if (e.LeftButton != MouseButtonState.Pressed)
            {
                ResizableBorder_MouseUp(null, null);
            }
            
            e.Handled = true;
        
 
            Point origPosition = new Point(Canvas.GetLeft(resizable.ResizableControl), Canvas.GetTop(resizable.ResizableControl));
            ResizeEventArgs args = new ResizeEventArgs(newMousePos, resize_dir);

            Console.WriteLine("MouseMove, mouse captured, attempting to move: resize_dir: " + resize_dir);

            Point deltaPoint = new Point(newMousePos.X - last_drag_position.X, newMousePos.Y - last_drag_position.Y);
            last_drag_position = newMousePos;

            if (resize_dir.HasFlag(ResizeDirection.N))
            {
                Console.WriteLine("Has North: increasing top and reducing height by " + deltaPoint.Y);
                //set Top to Y
                Canvas.SetTop(resizable.ResizableControl, origPosition.Y + deltaPoint.Y);
                //Add delta to height
                resizable.ResizableControl.Height -= deltaPoint.Y;
            }

           

            if (resize_dir.HasFlag(ResizeDirection.W))
            {
                //set Top to Y
                Canvas.SetLeft(resizable.ResizableControl, origPosition.X + deltaPoint.X);
                //subtract delta to width
                resizable.ResizableControl.Width -= deltaPoint.X;
                Console.WriteLine("Has West: SetLeft = " + newMousePos.X + " reducing width by " + deltaPoint.X);
            }

            if (resize_dir.HasFlag(ResizeDirection.S))
            {
                //Add delta to height
                resizable.ResizableControl.Height += deltaPoint.Y;
                Console.WriteLine("Has South: assigning height to " + deltaPoint.Y);
            }

            if (resize_dir.HasFlag(ResizeDirection.E))
            {
                //Add delta to width
                resizable.ResizableControl.Width += deltaPoint.X;
                Console.WriteLine("Has East: extending width by " + deltaPoint.X);
            }
        }

        private void ResizableBorder_MouseUp(object sender, MouseButtonEventArgs e)
        {
            resizable.ResizeBorder.ReleaseMouseCapture();
            Mouse.OverrideCursor = null;
            resize_dir = ResizeDirection.NONE;
            is_dragging = false;
        }
        private void ResizableBorder_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Point p = Mouse.GetPosition(resizable.ResizeBorder);
            if (IsEnabled && !is_dragging && IsOverBorder(p))
            {
                last_drag_position = p;
                is_dragging = true;
                Console.WriteLine("Mouse down on border, capturing mouse");
                resize_dir = GetResizeDir(last_drag_position);
                resizable.ResizeBorder.CaptureMouse();
            }
        }
    }
}
