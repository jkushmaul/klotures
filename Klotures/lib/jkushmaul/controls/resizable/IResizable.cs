﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Klotures.lib.jkushmaul.controls.resizable
{
    public interface IResizable
    {
        Border ResizeBorder { get;  }
        Control ResizableControl { get; }

    }
}
