﻿using System;
using System.Windows.Forms;

namespace Klotures
{
    public class SysTray
    {
        public EventHandler Exit_OnClick { get; set; }
        public EventHandler Context_Popup { get; set; }

        private System.Windows.Forms.NotifyIcon trayIcon;
        

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>")]
        public SysTray(string name, string location, EventHandler Context_Popup, EventHandler Exit_OnClick)
        {
            trayIcon = new System.Windows.Forms.NotifyIcon();
            trayIcon.Icon = System.Drawing.Icon.ExtractAssociatedIcon(location);
            trayIcon.ContextMenu = new System.Windows.Forms.ContextMenu();
            trayIcon.Text = name;
            this.Exit_OnClick = Exit_OnClick;
            this.Context_Popup = Context_Popup;

            trayIcon.ContextMenu.MenuItems.Add("Exit", Exit_OnClick).Name = "exit";
        }

        public bool Visible
        {
            get { return trayIcon.Visible; }
            set { trayIcon.Visible = value; }
        }

        public void Dispose()
        {
            trayIcon.Visible = false;
            trayIcon.Dispose();
        }

        internal MenuItem AddMenuItem(string caption, EventHandler On_Click, string name)
        {
            MenuItem menuItem = new MenuItem(caption, On_Click);
            menuItem.Name = name;
            AddMenuItem(menuItem);
            return menuItem;
        }
        internal MenuItem AddMenuItem(string caption, EventHandler On_Click)
        {
            return AddMenuItem(caption, On_Click, caption.ToLower());
        }

        internal void AddMenuItem(MenuItem menuItem)
        {
            int index = trayIcon.ContextMenu.MenuItems.Count-1;
            if (index < 0)
            {
                index = 0;
            }
            trayIcon.ContextMenu.MenuItems.Add(index, menuItem);
        }
    }
}
