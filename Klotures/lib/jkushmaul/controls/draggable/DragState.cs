﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klotures.lib.jkushmaul.controls.draggable
{
    public enum DragState
    {
        NO_DRAG = 0,
        INIT_DRAG = 1,
        DO_DRAG = 2
    }
}
