﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Klotures.lib.jkushmaul.controls.draggable
{
    public class DraggableEventArgs
    {
        public enum CompletionState {
            COMPLETE, INPROGRESS, CANCELED
        }
        public CompletionState state { get; }
        public Point NewPosition { get;}
        public Point OriginalPosition { get;}
        public DraggableEventArgs(CompletionState state, Point newPosition, Point originalPosition)
        {
            this.state = state;
            this.NewPosition = newPosition;
            this.OriginalPosition = originalPosition;
        }
    }
}
