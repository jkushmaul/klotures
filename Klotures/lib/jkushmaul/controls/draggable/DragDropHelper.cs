﻿using System;
using System.Windows;
using System.Windows.Input;

namespace Klotures.lib.jkushmaul.controls.draggable
{
    /// <summary>
    /// Draggable handles drag and drop for controls.
    /// </summary>
    class DragDropHelper
    {

        public DragState State { get; protected set; }
        public Point OriginalLocation { get; protected set; }
        public double DragInitDistance { get; set; }
        public bool Enabled { get; set; }
        public event EventHandler OnDragInitData;
        public event EventHandler<DraggableEventArgs> OnDragEvent;


        
        private Point startMousePosition;
        private Point lastMousePosition;
        private IDraggable dragHost;
        private IInputElement targetElement;

        public DragDropHelper(IDraggable dragHost, IInputElement targetElement)
        {
            State = DragState.NO_DRAG;
            DragInitDistance = 4.0;
            Enabled = true;
            this.dragHost = dragHost;
            this.targetElement = targetElement;
            this.targetElement.MouseMove += this.Draggable_MouseMove;
        }

        /// <summary>
        /// Gets the current mouse position on screen
        /// </summary>
        private Point MousePosition
        {
            get
            {
                // Position of the mouse relative to the window
                var position = Mouse.GetPosition(dragHost);
                // Add the window position
                return new Point(position.X + dragHost.Position.X, position.Y + dragHost.Position.Y);
            }
        }

        private Point DragHostPosition()
        {
            Point p = MousePosition;
            p.X += dragHost.Position.X - lastMousePosition.X;
            p.Y += dragHost.Position.Y - lastMousePosition.Y;
            return p;
        }

        private void FireOnDragInitData()
        {
            if (OnDragInitData != null)
            {
                OnDragInitData(dragHost, EventArgs.Empty);
            }
        }
        private void FireOnDragMove()
        {
            if (OnDragEvent != null)
            {
                OnDragEvent(dragHost, new DraggableEventArgs(DraggableEventArgs.CompletionState.INPROGRESS , DragHostPosition(), OriginalLocation));
            }
        }
        private void FireOnDragCancel()
        {
            if (OnDragEvent != null)
            {
                OnDragEvent(dragHost, new DraggableEventArgs(DraggableEventArgs.CompletionState.CANCELED, DragHostPosition(), OriginalLocation));
            }
        }
        private void FireOnDragComplete()
        {
            if (OnDragEvent != null)
            {
                OnDragEvent(dragHost, new DraggableEventArgs(DraggableEventArgs.CompletionState.COMPLETE, DragHostPosition(), OriginalLocation));
            }
        }

        public void StopMove()
        {
            if (State == DragState.NO_DRAG)
            {
                return;
            }
            State = DragState.NO_DRAG;
        }

        private void CancelMove()
        {
            StopMove();
            FireOnDragCancel();
        }

        private void Draggable_MouseMove(object sender, MouseEventArgs e)
        {
            if (!Enabled)
            {
                return;
            }
            if (e.LeftButton != MouseButtonState.Pressed || e.RightButton == MouseButtonState.Pressed)
            {
                targetElement.ReleaseMouseCapture();
                if (State == DragState.DO_DRAG)
                {
                    StopMove();
                    FireOnDragComplete();
                } else if (State != DragState.NO_DRAG)
                {
                    CancelMove();
                }
                return;
            }

            var position = MousePosition; 
            if (State == DragState.NO_DRAG)
            {
                startMousePosition = position;
                State++;
                return;
            } if (State == DragState.INIT_DRAG)
            {
                Vector distance = startMousePosition - position;
                if (distance.Length < 4)
                {
                    return;
                }
                State++;

                OriginalLocation = dragHost.Position;
                lastMousePosition = position;
                targetElement.CaptureMouse();
                FireOnDragInitData();
                return;
            } else
            {
                FireOnDragMove();
                lastMousePosition = position;
            }
        }
    }
}
