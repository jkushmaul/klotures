﻿using System.Windows;

namespace Klotures.lib.jkushmaul.controls.draggable
{
    public interface IDraggable : IInputElement
    {
        Point Position { get;}
    }
}
