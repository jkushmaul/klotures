﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klotures.lib.jkushmaul
{
    /*
     * This is necessary for overflowing listview - the underlying collection must be sorted, so that Top(n) can be taken, the rest spilling.
     * 
     */
    public class SortedObservableCollection<T, C> : ObservableCollection<T>
        where C : IComparer<T>
    {
        private object sortingMutex = new object();
        private volatile int sortCount = 0;

        private C comparer;
        public C CustomSort
        {
            get => comparer;
            set
            {
                comparer = value;
                Sort();
            }
        }

        public SortedObservableCollection() : base() {}
        public SortedObservableCollection(List<T> list) : base(list) { }
        public SortedObservableCollection(IEnumerable<T> collection) : base(collection) { }

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            bool doUpdate = false;
            lock (sortingMutex)
            {
                doUpdate = sortCount == 0;
            }
            if (doUpdate)
            {
                Sort();
            }
        }

        

        public void Sort()
        {
            lock (sortingMutex)
            {
                if (sortCount != 0)
                {
                    //this causes the last one to finish, to execute the action.
                    return;
                }
                sortCount++;
            }

            if (comparer != null)
            {
                var sortableList = this.OrderBy(o => o, comparer).ToList();
                for (int i = 0; i < sortableList.Count; i++)
                {
                    int sortedIndex = IndexOf(sortableList[i]);
                    Move(sortedIndex, i);
                }
            }

            lock (sortingMutex)
            {
                sortCount--;
            }
            NotifyCollectionChangedEventArgs e = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset);
            base.OnCollectionChanged(e);
        }

       
    }
}
