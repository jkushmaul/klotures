﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klotures.lib.jkushmaul
{
    public class StringEventArgs : EventArgs
    {
        public readonly string arg;
        public StringEventArgs(string arg)
        {
            this.arg = arg;
        }
    }
}
