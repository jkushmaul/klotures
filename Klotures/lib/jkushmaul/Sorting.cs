﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klotures.lib.jkushmaul
{
    public enum SortDirection
    {
        Ascending, Descending
    }
    public enum SortMethod
    {
        None,
        Alphabetical,
        Frequency
    }

    public class SortComparer : IComparer
    {

        private Func<object, object, int> compare;
        private SortDirection direction;
        public SortComparer(SortDirection direction, Func<object, object, int> compare)
        {
            this.direction = direction;
            this.compare = compare;
        }
        public int Compare(object x, object y)
        {
            int mod = direction == SortDirection.Ascending ? 1 : -1;
            return mod * this.compare(x, y);
        }
    }

    public class GenericSortComparer<T> : IComparer<T>
        where T: IComparable
    {
        private IComparer comparer;
        public GenericSortComparer(IComparer comparer)
        {
            this.comparer = comparer;
        }

        public int Compare(T x, T y)
        {
            return comparer.Compare(x, y);
        }
    }
}
