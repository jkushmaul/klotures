﻿using Klotures.win32;
using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using static Klotures.lib.win32.flags.NativeFlags;

namespace Klotures.lib.win32.controls
{

    internal class NativeMemory : IDisposable
    {
        private int vSize = 0;
        public int Size { get { return vSize; } }
        private IntPtr vPointer = IntPtr.Zero;
        public IntPtr Pointer { get { return vPointer; } }

        public IntPtr WithOffset(int offset)
        {
            if (offset > vSize)
            {
                throw new IndexOutOfRangeException("The offset requested was outside of this NativeAllocation");
            }
            return vPointer + offset;
        }

        public NativeMemory(byte[] buffer) : this(buffer.Length)
        {
            Marshal.Copy(buffer, 0, vPointer, vSize);
        }

        public NativeMemory(int size)
        {
            vSize = size;
            vPointer = Marshal.AllocHGlobal(vSize);
            if (vPointer == IntPtr.Zero)
            {
                throw new OutOfMemoryException();
            }
        }

        public virtual void Dispose()
        {
            if (vPointer != IntPtr.Zero)
            {
                Marshal.FreeHGlobal(vPointer);
                vPointer = IntPtr.Zero;
            }
        }

        void ThrowOnMemoryAccessViolation(int offset, int size)
        {
            if (offset + size > vSize)
            {
                throw new IndexOutOfRangeException("The given offset and size was outside of this NativeAllocation");
            }
        }

        public UInt32 ReadUInt32(int offset)
        {
            int size = Marshal.SizeOf(typeof(Int32));
            ThrowOnMemoryAccessViolation(offset, size);
            Int32 result = Marshal.ReadInt32(WithOffset(offset));
            return (UInt32)result;
        }

    }

    internal class RemoteMemory : IDisposable
    {
        internal class RemoteProcess
        {
            private IntPtr vProcess = IntPtr.Zero;
            public IntPtr Pointer { get { return vProcess; } }
            public RemoteProcess(IntPtr vProcess)
            {
                this.vProcess = vProcess;
            }
        }
        private RemoteProcess vProcess;
        private IntPtr vPointer;
        public IntPtr Pointer { get { return vPointer; } }
        private int vSize;

        public RemoteMemory(RemoteProcess vProcess, int size) : base()
        {
            this.vProcess = vProcess;
            vSize = size;

            vPointer = NativeMethods.VirtualAllocEx(vProcess.Pointer, IntPtr.Zero, (uint)vSize,
                                AllocationType.Reserve | AllocationType.Commit, MemoryProtection.ReadWrite);
        }
        internal static RemoteMemory CreateNativeAllocation<T>(RemoteProcess vProcess, T[] arr)
        {
            RemoteMemory rm = new RemoteMemory(vProcess, (Marshal.SizeOf(typeof(T)) * arr.Length));
            rm.WriteToNative(arr, 0);
            return rm;
        }

        public virtual void Dispose()
        {
            if (vPointer != IntPtr.Zero)
            {
                if (!NativeMethods.VirtualFreeEx(vPointer, vPointer, 0, AllocationType.Release))
                {
                    int lastError = Marshal.GetLastWin32Error();
                    vPointer = IntPtr.Zero;
                    if (lastError != 0)
                    {
                        //throw new Win32Exception(lastError);
                    }
                }

            }
        }

        public IntPtr WithOffset(int offset)
        {
            if (offset > vSize)
            {
                throw new IndexOutOfRangeException("The offset requested was outside of this allocation");
            }
            return vPointer + offset;
        }

        public int ReadFromNative(int offset, byte[] vBuffer)
        {
            int vNumberOfBytesRead = 0;
            IntPtr readPtr = WithOffset(offset);
            if (!NativeMethods.ReadProcessMemory(vProcess.Pointer, readPtr,
              Marshal.UnsafeAddrOfPinnedArrayElement(vBuffer, 0),
              vBuffer.Length, ref vNumberOfBytesRead))
            {
                int lastError = Marshal.GetLastWin32Error();
                if (lastError != 0)
                {
                    throw new Win32Exception(lastError);
                }
            }
            return vNumberOfBytesRead;
        }
        public int WriteToNative<T>(T[] itemArr, int offset)
        {
            int vNumberOfBytesWritten = 0;
            IntPtr writePtr = WithOffset(offset);
            if (!NativeMethods.WriteProcessMemory(vProcess.Pointer, writePtr,
                       Marshal.UnsafeAddrOfPinnedArrayElement(itemArr, 0),
                       Marshal.SizeOf(typeof(T)) * itemArr.Length, ref vNumberOfBytesWritten))
            {
                int lastError = Marshal.GetLastWin32Error();
                if (lastError != 0)
                {
                    throw new Win32Exception(lastError);
                }
            }
            return vNumberOfBytesWritten;
        }



        internal int ReadFromNative<T>(int offset, T[] arr)
        {
            int vNumberOfBytesRead = 0;
            IntPtr readPtr = WithOffset(offset);
            if (!NativeMethods.ReadProcessMemory(vProcess.Pointer, readPtr,
                    Marshal.UnsafeAddrOfPinnedArrayElement(arr, 0),
                    Marshal.SizeOf(typeof(T)) * arr.Length, ref vNumberOfBytesRead))
            {
                int lastError = Marshal.GetLastWin32Error();
                if (lastError != 0)
                {
                    throw new Win32Exception(lastError);
                }
            }
            return vNumberOfBytesRead;
        }
    }
}
