﻿using Klotures.lib.SharpShellHidden;
using Klotures.lib.win32.flags;
using Klotures.lib.win32.structs;
using Klotures.win32;
using SharpShell.Interop;
using SharpShell.Pidl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Interop; 

namespace Klotures.lib.jkushmaul
{
    /// <summary>
    /// From https://docs.microsoft.com/en-us/previous-versions/windows/desktop/legacy/dd940348(v=vs.85)
    /// </summary>
    class ShellNotifier
    {
        const uint WM_SHELLNOTIFY = (uint)NativeFlags.WindowMessages.WM_USER + 0x01;

        private ShellNotifierWindow child;
        private ShellItem parentFolder;

        public NativeFlags.SHCNE MessageSubscriptions { get; set; }
        public event EventHandler<ShellEventArg> OnShellMessage;

        public ShellNotifier(ShellItem shellItem)
        {
            MessageSubscriptions = 0;
            parentFolder = shellItem;
        }

        public void Register()
        {
            if (child != null)
            {
                throw new Exception("Already registered");
            }
            child = new ShellNotifierWindow(this);
            child.Show();
        }
        public void Unregister()
        {
            if (child == null)
            {
                return;
            }
            child.Close();
            child = null;
        }


        private class ShellNotifierWindow : Window
        {
            public uint _ulRegister { get; set; }

        
            private const int HWND_MESSAGE = -3;
            private HwndSource source;
            private ShellNotifier parent;
            public ShellNotifierWindow(ShellNotifier parent)
            {
                this.parent = parent;
                this.Visibility = Visibility.Collapsed;
            }
            protected override void OnSourceInitialized(EventArgs e)
            {
                base.OnSourceInitialized(e);
                source = PresentationSource.FromVisual(this) as HwndSource;
                source.AddHook(WndProc);

                NativeMethods.SetParent(source.Handle, (IntPtr)HWND_MESSAGE);
                Visibility = Visibility.Hidden;



                register();
            }
            protected override void OnClosing(CancelEventArgs e)
            {
                unregister();
                base.OnClosing(e);
            }
            protected void unregister()
            {
                if (_ulRegister != 0)
                {
                    NativeMethods.SHChangeNotifyDeregister(_ulRegister);
                }
            }

            protected void register()
            {
                if (_ulRegister != 0)
                {
                    throw new Exception("Already registered");
                }
                
                NativeStructs.SHChangeNotifyEntry entry = new NativeStructs.SHChangeNotifyEntry();
                entry.Recursively = false;
                entry.pIdl = parent.parentFolder.PIDL;

                _ulRegister = NativeMethods.SHChangeNotifyRegister(
                        source.Handle,
                        //NativeFlags.SHCNRF.InterruptLevel | NativeFlags.SHCNRF.NewDelivery | NativeFlags.SHCNRF.ShellLevel,
                        NativeFlags.SHCNF.SHCNF_TYPE | NativeFlags.SHCNF.SHCNF_IDLIST,
                        NativeFlags.SHCNE.SHCNE_ALLEVENTS | NativeFlags.SHCNE.SHCNE_INTERRUPT,
                        WM_SHELLNOTIFY,
                        1,
                        ref entry);
                if (_ulRegister == 0)
                {
                    throw new Win32Exception("Could not register shell notification");
                }
            }
            
            
            private  IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
            {
                if (_ulRegister != 0 && msg == WM_SHELLNOTIFY)
                {
                    //and lParam specifies the event.  
                    NativeFlags.SHCNE lparamEv = (NativeFlags.SHCNE)lParam;
                    parent.HandleMessage(lparamEv, wParam, ref handled);
                }
                return IntPtr.Zero;
            }
        }


        private void HandleMessage(NativeFlags.SHCNE et, IntPtr wParam, ref bool handled)
        {
            if (wParam == IntPtr.Zero)
            {
                return;
            }
            if (et.HasFlag(NativeFlags.SHCNE.SHCNE_CREATE))
            {

            }
            else if (et.HasFlag(NativeFlags.SHCNE.SHCNE_DELETE))
            {

            }
            else if (et.HasFlag(NativeFlags.SHCNE.SHCNE_MKDIR))
            {

            }
            else if (et.HasFlag(NativeFlags.SHCNE.SHCNE_RENAMEFOLDER))
            {

            }
            else if (et.HasFlag(NativeFlags.SHCNE.SHCNE_RENAMEITEM))
            {

            }
            else if (et.HasFlag(NativeFlags.SHCNE.SHCNE_RMDIR))
            {

            }
            else if (et.HasFlag(NativeFlags.SHCNE.SHCNE_UPDATEDIR))
            {

            }
            else if (et.HasFlag(NativeFlags.SHCNE.SHCNE_UPDATEIMAGE))
            {

            }
            else if (et.HasFlag(NativeFlags.SHCNE.SHCNE_UPDATEITEM))
            {

            }
            else
            {
                //anything else is not relevant to what I need
                return;
            }


            

            ShellItem[] shellItems = { null, null };

            var items = (NativeStructs.ShellNotifyStruct)Marshal.PtrToStructure(wParam, typeof(NativeStructs.ShellNotifyStruct));

            StringBuilder path1 = new StringBuilder(256);
            if (items.item1 != IntPtr.Zero)
            {
                NativeMethods.SHGetPathFromIDList(items.item1, path1);
            }
            StringBuilder path2 = new StringBuilder(256);
            if (items.item2 != IntPtr.Zero)
            {
                NativeMethods.SHGetPathFromIDList(items.item2, path2);
            }

            if (path1.Length == 0 && path2.Length == 0) 
            { 
                return;
            }

            ShellEventArg args = new ShellEventArg(et, path1.ToString(), path2.ToString());
            OnShellMessage(this, args);
            handled = args.Handled;
        }


        public class ShellEventArg
        { 
            public NativeFlags.SHCNE EventType { get; protected set; }
            public string path1;
            public string path2;
            public bool Handled { get; set; }
            public ShellEventArg(NativeFlags.SHCNE EventType, string path1, string path2)
            {
                this.EventType = EventType;
                this.path1 = path1;
                this.path2 = path2;
                this.Handled = false;
            }
        }
    }
}
