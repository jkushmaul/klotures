﻿using Klotures.lib.win32.flags;
using Klotures.win32;
using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;

namespace Klotures.lib.jkushmaul
{
    public static class DesktopContext
    {
        private static IntPtr hProgMan = IntPtr.Zero;
        private static IntPtr hWorkerW = IntPtr.Zero;
        private static IntPtr hShellDefView = IntPtr.Zero;
        private static IntPtr hSysListView32 = IntPtr.Zero;
        private static IntPtr hSysHeader32 = IntPtr.Zero;


        public static IntPtr ProgMan
        {
            get
            {
                Create();
                return hWorkerW == IntPtr.Zero ? hProgMan : hWorkerW;
            }
        }
        public static IntPtr ShellDefView { get { Create(); return hShellDefView; } }
        public static IntPtr WorkerW { get { Create(); return hWorkerW; } }
        public static IntPtr SysListView32 { get { Create(); return hSysListView32; } }
        public static IntPtr SysHeader32 { get { Create(); return hSysHeader32; } }

        private static object SyncLock = new object();

        private static IntPtr FindWindow(string name)
        {
            return FindWindow(name, true);
        }
        private static IntPtr FindWindow(string name, bool throwOnNull)
        {
            IntPtr hwnd = NativeMethods.FindWindow(name, null);
            int lastError = Marshal.GetLastWin32Error();
            if (throwOnNull && hwnd == IntPtr.Zero)
            {
                throw new DllNotFoundException("Could not FindWindow(" + name + ")", new Win32Exception(lastError));
            }
            return hwnd;
        }


        private static IntPtr FindWindowEx(string name, IntPtr parent)
        {
            return FindWindowEx(name, parent, true);
        }
        private static IntPtr FindWindowEx(string name, IntPtr parent, bool throwOnNull)
        {
            IntPtr hwnd = NativeMethods.FindWindowEx(parent, IntPtr.Zero, name, null);
            int lastError = Marshal.GetLastWin32Error();
            if (throwOnNull && hwnd == IntPtr.Zero)
            {
                throw new DllNotFoundException("Could not FindWindowEx(" + name + "...)", new Win32Exception(lastError));
            }
            return hwnd;
        }




        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>")]
        public static IntPtr Get()
        {
            if (hSysListView32 == IntPtr.Zero)
            {
                lock (SyncLock)
                {
                    if (hSysListView32 == IntPtr.Zero)
                    {
                        Create();
                    }
                }
            }
            return hSysListView32;
        }


        private static void LookForWorker()
        {
            // We enumerate all Windows, until we find one, that has the SHELLDLL_DefView 
            // as a child. 
            // If we found that window, we take its next sibling and assign it to workerw.

            bool result = NativeMethods.EnumWindows(new NativeMethods.EnumWindowsProc((tophandle, topparamhandle) =>
            {
                IntPtr p = FindWindowEx("SHELLDLL_DefView", tophandle, false);

                if (p != IntPtr.Zero)
                {
                    hShellDefView = p;
                    p = FindWindowEx("SysListView32", hShellDefView, false);
                    if (p != IntPtr.Zero)
                    {
                        hSysListView32 = p;
                        hWorkerW = NativeMethods.GetParent(hShellDefView);
                        return true;
                    }
                }
                return true;
            }), IntPtr.Zero);

        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>")]
        private static void Create()
        {
            // Find the desktop window
            hProgMan = FindWindow("ProgMan");
            hShellDefView = FindWindowEx("SHELLDLL_DefView", hProgMan, false);
            if (hShellDefView != IntPtr.Zero)
            {
                hSysListView32 = FindWindowEx("SysListView32", hShellDefView);
            }
            else
            {
                LookForWorker();
            }
            if (hSysListView32 == IntPtr.Zero)
            {
                throw new Win32Exception("Could not attach to desktop, SysListView32 not found");
            }
            hSysHeader32 = FindWindowEx("SysHeader32", hSysListView32);
            if (hSysHeader32 == IntPtr.Zero)
            {
                throw new Win32Exception("Could not attach to desktop, SysHeader32 not found");
            }
        }



        public static void HideFromTaskSwitcher(System.Windows.Window window)
        {
            WindowInteropHelper wndHelper = new WindowInteropHelper(window);

            int exStyle = (int)NativeMethods.GetWindowLong(wndHelper.Handle, (int)NativeFlags.GetWindowLongFields.GWL_EXSTYLE);

            exStyle |= (int)NativeFlags.ExtendedWindowStyles.WS_EX_TOOLWINDOW;
            NativeMethods.SetWindowLong(wndHelper.Handle, (int)NativeFlags.GetWindowLongFields.GWL_EXSTYLE, (IntPtr)exStyle);
        }


        public static void SetIconsVisible(bool wantVisible)
        {

            if (wantVisible)
            {
               NativeMethods.ShowWindow(DesktopContext.SysListView32, 5);
            }
            else
            {
               NativeMethods.ShowWindow(DesktopContext.SysListView32, 0);
            }
        }

        /// <summary>
        /// Gets the current mouse position on screen
        /// </summary>
        public static System.Windows.Point GetMousePosition(System.Windows.Window window)
        {
            // Position of the mouse relative to the window
            var position = Mouse.GetPosition(window);

            // Add the window position
            return new System.Windows.Point(position.X + window.Left, position.Y + window.Top);
        }


        public static Rect GetDesktopBounds()
        {
            int new_width = 0;
            int new_height = 0;


            int x = 0, y = 0;
            int i = 0;
            foreach (System.Windows.Forms.Screen screen in System.Windows.Forms.Screen.AllScreens)
            {
                if (i == 0 || screen.Bounds.X < x)
                {
                    x = screen.Bounds.X;
                }
                if (i == 0 || screen.Bounds.Y < y)
                {
                    y = screen.Bounds.Y;
                }
                
                
                new_width += screen.Bounds.Width;
                if (new_height < screen.Bounds.Height)
                {
                    new_height = screen.Bounds.Height;
                }
                i++;
            }
            return new Rect(x, y, new_width, new_height);
        }
    }
}
