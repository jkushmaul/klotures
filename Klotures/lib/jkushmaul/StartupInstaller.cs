﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Klotures.lib.jkushmaul
{
    class StartupInstaller
    {
        private const string startupPath = @"%APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup";
        private const string startupLinkFile = "Klotures.lnk";

        public bool HasStartupInstalled()
        {
            return System.IO.File.Exists(GetStartupPath());
        }

        public string GetStartupPath()
        {
            return System.IO.Path.Combine(Environment.ExpandEnvironmentVariables(startupPath), startupLinkFile);
        }

        internal void DeleteShortcut()
        {
            System.IO.File.Delete(GetStartupPath());
        }

        internal void InstallShortcut()
        {
            IWshRuntimeLibrary.WshShell shell = new IWshRuntimeLibrary.WshShell();
            var shortcut_path = GetStartupPath();
            var shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(shortcut_path);
            shortcut.Description = "Klotures";
            shortcut.WorkingDirectory = Application.StartupPath;
            shortcut.TargetPath = Application.ExecutablePath;
            shortcut.Save();
            shortcut = null;
            shell = null;
        }
    }
}
