﻿using Klotures.lib.jkushmaul;
using Klotures.lib.SharpShellHidden;
using Klotures.lib.win32.flags;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Klotures
{
    public class MainIconShellInterface
    {
        private ShellNotifier shellEventNotifier;
        private MainIconWindow mainIconWindow;
        private BackgroundWorker backgroundWorker;

        public MainIconShellInterface(MainIconWindow mainIconWindow)
        {
            this.mainIconWindow = mainIconWindow;

            backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += ShellItems_DoWork;
        }

        public void Shutdown()
        {
            if (shellEventNotifier != null)
            {
                shellEventNotifier.Unregister();
                shellEventNotifier = null;
            }

        }

        public void Startup()
        {
            this.shellEventNotifier = new ShellNotifier(ShellItem.DesktopShellFolder);
            shellEventNotifier.OnShellMessage += ShellEventNotifier_OnShellMessage;
            shellEventNotifier.MessageSubscriptions = NativeFlags.SHCNE.SHCNE_ALLEVENTS;
            shellEventNotifier.Register();

            backgroundWorker.RunWorkerAsync();
        }

        private void ShellEventNotifier_OnShellMessage(object sender, ShellNotifier.ShellEventArg e)
        {

            switch (e.EventType)
            {
                case NativeFlags.SHCNE.SHCNE_RMDIR:
                case NativeFlags.SHCNE.SHCNE_DELETE:
                    Shell_OnItemDeleted(e.path1);
                    break;
                case NativeFlags.SHCNE.SHCNE_MKDIR:
                case NativeFlags.SHCNE.SHCNE_CREATE:
                    Shell_OnItemCreated(e.path1);
                    break;
                case NativeFlags.SHCNE.SHCNE_RENAMEFOLDER:
                case NativeFlags.SHCNE.SHCNE_RENAMEITEM:
                    Shell_OnItemRenamed(e.path1, e.path2);
                    break;
                case NativeFlags.SHCNE.SHCNE_UPDATEDIR:
                case NativeFlags.SHCNE.SHCNE_UPDATEITEM:
                    Shell_OnItemUpdate(e.path1);
                    break;
                default:
                    Console.WriteLine("Shell notification: EventType: " + e.EventType);
                    Console.WriteLine("path1: " + e.path1);
                    Console.WriteLine("path2: " + e.path2);
                    break;
            }
        }


        private void Shell_OnItemUpdate(string path)
        {
            //this is called when parent folder has an item deleted (along with the deleted event too)
            //it is also called on icon update.
            IconControl ic = FindIcon(path);
            if (ic != null)
            {
                ShellItem shellItem = ShellItem.DesktopShellFolder.GetChildren(ShellItem.ChildTypes.Files | ShellItem.ChildTypes.Folders).FirstOrDefault(i => i.Path == path);
                if (shellItem != null)
                {
                    ic.UpdateShellItem(shellItem);
                }
            }
        }
        private void Shell_OnItemRenamed(string path1, string path2)
        {
            Console.WriteLine("Item Renamed: " + path1 + " -> " + path2);
            IconControl ic = FindIcon(path1);
            if (ic != null)
            {
                ic.ParentIconContainer.RenameShortcut(ic, path2);
            }
        }
        private void Shell_OnItemCreated(string path)
        {
            Console.WriteLine("Item Created: " + path);
            ShellItem shellItem = ShellItem.DesktopShellFolder.GetChildren(ShellItem.ChildTypes.Files | ShellItem.ChildTypes.Folders).FirstOrDefault(i => i.Path == path);
            if (shellItem != null)
            {
                mainIconWindow.DesktopIconWindow.AddShortcut(shellItem, true);
            }
        }
        private void Shell_OnItemDeleted(string path)
        {
            IconControl ic = FindIcon(path);
            if (ic != null)
            {
                ic.ParentIconContainer.RemoveShortcut(ic);
                ic.Dispose();
            }
        }


        private void ShellItems_DoWork(object sender, DoWorkEventArgs e)
        {
            List<ShellItem> shellItems = ((App)Application.Current).ShellItems.Values.ToList();
            foreach (IconWindow iw in mainIconWindow.Containers.Values)
            {
                iw.TakeShellItems(ref shellItems);
            }

            //add the remaining to the desktopWindow.
            Application.Current.Dispatcher.BeginInvoke(
                    DispatcherPriority.Background,
                    new Action(() => {
                        mainIconWindow.DesktopIconWindow.TakeShellItems(ref shellItems);
                    })
                    );
        }

        private IconControl FindIcon(string path)
        {
            foreach (IconWindow iw in mainIconWindow.Containers.Values)
            {
                if (iw.HasShortcut(path))
                {
                    return iw.GetShortcut(path);
                }
            }
            return null;
        }
    }
}
