﻿using Klotures.lib.jkushmaul;
using Klotures.lib.jkushmaul.controls;
using Klotures.lib.SharpShellHidden;
using Klotures.settings;
using Peter;
using SharpShell.Interop;
using SharpShell.Pidl;
using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace Klotures
{


    /// <summary>
    /// Interaction logic for IconControl.xaml
    /// </summary>
    public partial class IconControl : UserControl, IDisposable
    {
        public IconWindow ParentIconContainer { get; set; }

        public string Path { get { return ShellItem.Path; } }
        public int Rank { get; set; }
        public string DisplayName { get { return ShellItem.DisplayName; } }
        public ShellItem ShellItem { get; private set; }
        public int ActivationCount { get; private set; }

        private ShellContextMenu shellContextMenu;
        public IconControl(IconWindow parentIconContainer, ShellItem shellItem) : this(parentIconContainer, shellItem, 0)
        {
        }
        public IconControl(IconWindow parentIconContainer, ShellItem shellItem, int ActivationCount)
        {
            this.ActivationCount = ActivationCount;
            InitializeComponent();
            ParentIconContainer = parentIconContainer;
    
            
            this.Width = 90;
            this.Height = 60;
            Image.Width = 48;
            Image.Height = 48;
            Image.ToolTip = new ToolTip();
            ShellItem = shellItem;

            UpdateShellItem();
            shellContextMenu = new ShellContextMenu();
        }


        internal void UpdateShellItem(ShellItem shellItem)
        {
            if (this.ShellItem != null)
            {
                this.ShellItem.Dispose();
                this.ShellItem = null;
            }
            this.ShellItem = shellItem; 
            UpdateShellItem();
            this.ParentIconContainer.RefreshView();
        }
        internal void UpdateShellItem()
        {
            Text.Text = ShellItem.DisplayName;
            ToolTip tt = (ToolTip)Image.ToolTip;
            tt.Content = DisplayName;
            Image.ToolTip = tt;

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += UpdateIcon_DoWork; 
            worker.RunWorkerAsync( );
        }

        private void UpdateIcon_DoWork(object sender, DoWorkEventArgs e)
        {
            object preheatPath = ShellItem.Path;
            int iconIndex = ShellItem.IconIndex;
            System.Drawing.Icon ico = ShellItem.OverlayIcon;
            ImageSource img = ImageTools.ToImageSource(ico);
            img.Freeze();

            Application.Current.Dispatcher.BeginInvoke(
                  DispatcherPriority.Background,
                  new Action(() => {
                      this.Image.Source = img;
                      double imgHeight = img.Height;
                      double txtHeight = this.Text.Height;
                      this.Height = imgHeight + txtHeight;
                  })
                  ); 
        }

        private void ActivateShortcut()
        {
            this.ActivationCount++;
            Shell32.Shell shell = new Shell32.Shell();
            Shell32.Folder desktop32 = shell.NameSpace(0);
           
            if (Path.StartsWith("Special/"))
            {
                shell.Open(ShellItem.ShellFolderInterface);
            } else
            {
                shell.Open(Path);
            }
           
            this.ParentIconContainer.RefreshView();
        }
 

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {

                if (e.ClickCount > 1)
                {
                    ActivateShortcut();
                    ParentIconContainer.FireSettingsChanged();
                }
        
            }
        }

        private void Image_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            OpenItemContextMenuViaPeter(ShellItem);
        }

        public void OpenItemContextMenuViaPeter(ShellItem itemHit)
        {
            var r = Mouse.GetPosition(this);
            Point p = PointToScreen(r);
            System.Drawing.Point dp = new System.Drawing.Point((int)p.X, (int)p.Y);
            shellContextMenu.ShowContextMenu(itemHit, dp);
        }

 

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    if (shellContextMenu != null)
                    {
                        shellContextMenu.DestroyHandle();
                        shellContextMenu = null;
                    }
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.
                ShellItem.Dispose();
                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~IconControl()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
         
        }
        #endregion
    } // End class
} //End namespace
