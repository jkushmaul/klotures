﻿using Klotures.lib.SharpShellHidden;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;

namespace Klotures
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        public Dictionary<string, ShellItem> ShellItems { get; protected set; }

        /// <summary>
        /// I had problems doing these initializations in a background worker, and getting them back through Dispatcher for Url type icons
        /// Moving it here prevents a visual delay with GUI (But obvious visual delay before the app is visible.  Once visible it is immediately
        /// responsive while loading icons.  Some work left but this removes that issue for me enough to leave it here.  The issue was just URL type
        /// icons loading as white.  No error, exception, missed errors - just a white icon.
        /// </summary>
        public App()
        {
            ShellItems = new Dictionary<string, ShellItem>();
            foreach (ShellItem childItem in ShellItem.DesktopShellFolder.GetChildren(ShellItem.ChildTypes.Files | ShellItem.ChildTypes.Folders))
            {
                object preheatPath = childItem.Path;
                int iconIndex = childItem.IconIndex;
                System.Drawing.Icon ico = childItem.OverlayIcon;
                IntPtr pidl = childItem.PIDL;

                string displayname = childItem.DisplayName;

                if (!ShellItems.ContainsKey(childItem.Path))
                {
                    ShellItems.Add(childItem.Path, childItem);
                }
            }
        }
        private void Application_Startup(object sender, StartupEventArgs e)
        {

        }

        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            string message = "";
            if (e != null)
            {
                if (e.Exception != null)
                {
                    message += "Message: " + e.Exception.Message + "\nStackTrace: " + e.Exception.StackTrace;
                }
                else
                {
                    message += "Null stack trace";
                }
            } else
            {
                message += "Null event args";
            }
            e.Handled = false;
            using (EventLog eventLog = new EventLog("Application"))
            {
                eventLog.Source = "Application";
                eventLog.WriteEntry(message, EventLogEntryType.Error);
            }
           
        }
    }
}
