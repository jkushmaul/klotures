﻿using Klotures.lib.jkushmaul;
using Klotures.settings;
using Klotures.win32;
using System;
using System.Collections.Generic; 
using System.Reflection;
using System.Windows;
using System.Windows.Interop;
using System.Linq;
using System.Configuration;
using System.Diagnostics;
using System.Windows.Controls;

namespace Klotures
{
    /// <summary>
    /// Interaction logic for DesktopIconContainer.xaml
    /// </summary>
    public partial class MainIconWindow : Window, IKloturesMetadataProvider
    {
        private KlotureSettingsManager klotureSettingsManager;
        private MainIconShellInterface shellInterface;

        private StartupInstaller startupInstaller;


        public bool IsOnStartup
        {
            get 
            {
                return startupInstaller.HasStartupInstalled(); 
            }
            set
            {
                if (value)
                {
                    startupInstaller.InstallShortcut();
                } else 
                {
                    startupInstaller.DeleteShortcut();
                }
            }
        }

        public bool IsGlobalFind
        {
            get
            {
                return klotureSettingsManager.GlobalFind;
            }
            set
            {
                klotureSettingsManager.Save(() =>
                {
                    klotureSettingsManager.GlobalFind = value;
                });
            }
        }

        public IconWindow DesktopIconWindow { get; private set; }

        public Dictionary<string, IconWindow> Containers { get; private set; }
        public KlotureMetadata[] KlotureMetaDatas
        {
            get
            {
                List<KlotureMetadata> list = new List<KlotureMetadata>();
                foreach(IconWindow iw in Containers.Values.ToArray()) {
                    list.Add(iw.ToMetadata());
                }
                return list.ToArray();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>")]
        public MainIconWindow()
        {
            startupInstaller = new StartupInstaller();

            shellInterface = null;
            Containers = new Dictionary<string, IconWindow>();
            klotureSettingsManager = new KlotureSettingsManager(this);

            InitializeComponent();
            Rect r = DesktopContext.GetDesktopBounds();
            this.Left = 0;
            this.Top = 0;
            this.Width = r.Width;
            this.Height = r.Height;


            shellInterface = new MainIconShellInterface(this);
        }


        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            DesktopContext.HideFromTaskSwitcher(this);
            NativeMethods.SetParent(new WindowInteropHelper(this).Handle, DesktopContext.ShellDefView);
        }

        protected override void OnLocationChanged(EventArgs e)
        {
            base.OnLocationChanged(e);
            var msg = "MainIconWindow.Location Changed: x=" + Left + ",y=" + Top;
            if (true)
            {
                if (Left < 0)
                {
                    Rect r = DesktopContext.GetDesktopBounds();
                    double new_x = -r.X;
                    msg += "; Resetting to " + new_x;
                    this.Left = new_x;
                }
                else
                {
                    msg += "; Not resetting";
                }
            }
            Debug.WriteLine(msg);
        }



        private void LoadFromSettings()
        {
            klotureSettingsManager.Lock(() =>
            {
                //Load saved settings, default may be included.
                IconWindow desktopNamedIw = null;
                foreach (KlotureMetadata m in klotureSettingsManager.KlotureMetaDatas)
                {
                    if (DesktopIconWindow != null)
                    {
                        m.IsDefault = false;
                    }
                    //Only add it if it is unique
                    //m.ID may be null if upgrading
                    if (m.ID == null || !Containers.ContainsKey(m.ID))
                    {
                        IconWindow b = IconWindow.FromMetadata(this, m);
                        if (b.IsDesktopWindow)
                        {
                            DesktopIconWindow = b;
                        }
                        System.Windows.Point p = b.Position;
                        if (double.IsNaN(p.Y) || double.IsNaN(p.X) ||
                            p.Y > Height || p.Y < 0 ||
                            p.X > Width || p.X < 0)
                        {
                            b.Position = new System.Windows.Point(0, 0);
                        }
                        AddContainer(b);
                        if (b.ContainerName == "Desktop")
                        {
                            desktopNamedIw = b;
                        }
                    }
                }


                //if no default, make one
                if (DesktopIconWindow == null)
                {
                    //prevent duplicate key issue
                    if (desktopNamedIw != null)
                    {
                        desktopNamedIw.IsDesktopWindow = true;
                        DesktopIconWindow = desktopNamedIw;
                    }
                    else
                    {
                        //no default, no duplicate key issue
                        DesktopIconWindow = new IconWindow(this);
                        DesktopIconWindow.ContainerName = "Desktop";
                        DesktopIconWindow.IsDesktopWindow = true;
                        AddContainer(DesktopIconWindow);
                        SaveSettings();
                    }
                }

                //now join overflows
                Dictionary<string, IconWindow> iconWindowsById = new Dictionary<string, IconWindow>();
                foreach (IconWindow iw in Containers.Values.ToArray())
                {
                    iconWindowsById[iw.ID] = iw;
                }
                //sanitize any missing Ids and link
                foreach (string ID in iconWindowsById.Keys.ToArray())
                {
                    IconWindow iw = iconWindowsById[ID];
                    if (!String.IsNullOrEmpty(iw.OverflowChildID))
                    {
                        if (!iconWindowsById.ContainsKey(iw.OverflowChildID))
                        {
                            iw.OverflowChildID = "";
                        }
                    }

                    if (!String.IsNullOrEmpty(iw.OverflowParentID))
                    {
                        if (!iconWindowsById.ContainsKey(iw.OverflowParentID))
                        {
                            iw.OverflowParentID = "";
                        }
                    }
                }
                //link the views
                foreach (IconWindow overflowFrom in iconWindowsById.Values.Where(iw => !String.IsNullOrEmpty(iw.OverflowChildID)))
                {
                    IconWindow overflowTo = iconWindowsById[overflowFrom.OverflowChildID];
                    LinkKlotures(overflowFrom, overflowFrom.OverflowLimit, overflowTo);
                }
            });
        }



        private void DesktopEmbeddedWindow_Loaded(object sender, RoutedEventArgs e)
        {           
            DesktopContext.SetIconsVisible(false);

            LoadFromSettings();

            shellInterface.Startup();

            if(klotureSettingsManager.FirstStartup)
            {
                StartupInstaller si = new StartupInstaller();
                if (!si.HasStartupInstalled())
                {
                    MessageBoxResult result = System.Windows.MessageBox.Show(
                   "Would you like to add Klotures to your startup folder?\nYou won't be asked again but can always manage this short cut from the try icon.", "Startup shortcut?",
                   MessageBoxButton.OKCancel);
                    if (result == MessageBoxResult.OK)
                    {
                        IsOnStartup = true;
                    }
                }

               klotureSettingsManager.Save(() =>
               {
                   klotureSettingsManager.FirstStartup = false;
               });
                
            }
            
        }

        private void SaveSettings()
        {
            klotureSettingsManager.Save(() => {

            });
        }

        internal bool RenameIconWindow(IconWindow iconWindow, string new_name)
        {
            klotureSettingsManager.Save(() =>
            {
               iconWindow.ContainerName = new_name;
            });

            return true;
        }

        private void DesktopEmbeddedWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            shellInterface.Shutdown();
            RemoveLogicalChild(TaskbarIcon);
            TaskbarIcon.Dispose();
            TaskbarIcon = null;

            DesktopContext.SetIconsVisible(true);
        }

        private void TrayMenuSettingsDir_OnClick(object sender, RoutedEventArgs e)
        {
            var path = System.IO.Path.GetDirectoryName(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath);
            System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo()
            {
                FileName = path,
                UseShellExecute = true,
                Verb = "open"
            });
        }

        private void TrayMenuOnStart_OnClick(object sender, RoutedEventArgs e)
        {
            
            if (startupInstaller.HasStartupInstalled())
            {
                MessageBoxResult result = System.Windows.MessageBox.Show(
                        "Are you sure you want to remove the startup shortcut?", "Confirm delete",
                        MessageBoxButton.OKCancel);
                if (result == MessageBoxResult.OK)
                {
                    this.IsOnStartup = false;
                }
            } else
            {
                MessageBoxResult result = System.Windows.MessageBox.Show(
                    "Are you sure you want to install a startup shortcut?", "Confirm install",
                    MessageBoxButton.OKCancel);
                if (result == MessageBoxResult.OK)
                {
                    this.IsOnStartup = true;
                }
            }
        }

        private void TrayMenuHideAll_Click(object sender, RoutedEventArgs e)
        {
            foreach (IconWindow iw in Containers.Values)
            {
                iw.SetVisible(false);
            }
        }
 

        private void TrayMenuShow_OnClick(object sender, RoutedEventArgs e)
        {
            MenuItem item = (MenuItem) sender;
            foreach (IconWindow iw in Containers.Values)
            {
                if (item.Header + "" == "All" || item.Tag + "" == iw.ID)
                {
                    iw.SetVisible(true);
                }
            }
        }

        private void TrayMenuShowAll_OnClick(object sender, RoutedEventArgs e)
        {
            foreach(IconWindow iw in Containers.Values)
            {
                iw.SetVisible(true);
            }
        }

        void TrayMenuCreate_OnClick(object sender, RoutedEventArgs e)
        {
            IconWindow iw = new IconWindow(this);
            iw.Position = new Point(0, 0);
            iw.Width = 600;
            iw.Height = 400;
            iw.OriginalHeight = 400;
            iw.ContainerName = "New Kloture";
            AddContainer(iw);
            SaveSettings();
        }

        void TrayMenuExit_OnClick(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }


        public void AddContainer(IconWindow iconContainer)
        {
            Containers.Add(iconContainer.ID, iconContainer);
            ContainerCanvas.Children.Add(iconContainer);
            if (iconContainer.IsDesktopWindow)
            {
                if (DesktopIconWindow != null && DesktopIconWindow != iconContainer)
                {
                    throw new Exception("MainIconWindow already has a default desktop window");
                }
                DesktopIconWindow = iconContainer;
            }
            
            if (double.IsNaN(iconContainer.Position.X) || double.IsNaN(iconContainer.Position.Y)) {
                iconContainer.Position = new Point(0, 0);
                iconContainer.Width = 100;
                iconContainer.Height = 100;
            } else
            {
                iconContainer.Position = iconContainer.Position;
            }

            iconContainer.SettingsChanged += IconContainer_SettingsChanged;
            iconContainer.FilterEnabled += IconContainer_FilterEnabled;
            iconContainer.FilterChanged += IconContainer_FilterChanged;
            iconContainer.FilterCancel += IconContainer_FilterCancel;
        }

        private void IconContainer_FilterCancel(object sender, EventArgs e)
        {
            if (klotureSettingsManager.GlobalFind)
            {
                foreach(IconWindow iw in Containers.Values)
                {
                    if (iw != sender)
                    {
                        iw.SetFindMode(false, false);
                    }
                }
            }
        }

        private void IconContainer_FilterChanged(object sender, EventArgs e)
        {
            StringEventArgs se = e as StringEventArgs;
            string filter = se.arg;
            if (klotureSettingsManager.GlobalFind)
            {
                foreach (IconWindow iw in Containers.Values)
                {
                    iw.SetFilter(filter);
                }
            } else
            {
                IconWindow iw = sender as IconWindow;
                iw.SetFilter(filter);
            }
        }

        private void IconContainer_FilterEnabled(object sender, EventArgs e)
        {
            if (klotureSettingsManager.GlobalFind)
            {
                foreach (IconWindow iw in Containers.Values)
                {
                    if (iw != sender)
                    {
                        iw.SetFindMode(true, false);
                    }
                }
            }
        }

        private void IconContainer_SettingsChanged(object sender, EventArgs e)
        {
            IconWindow iconWindow = sender as IconWindow;
            if (iconWindow != null)
            {
                if (iconWindow.IsDeleted)
                {
                    RemoveContainer(iconWindow);
                }
            }
            SaveSettings();
        }

        internal void RemoveContainer(IconWindow cloneOf)
        {
            Containers.Remove(cloneOf.ID);
        }

        internal bool WillCauseWindowOverlap(IconWindow iconWindow,  Point newP, Size newS)
        {
            foreach(IconWindow iw in Containers.Values)
            {
                if (iw != iconWindow)
                {
                    var rectA = new Rect(newP, newS);
                    var rectB = new Rect(iw.Position.X, iw.Position.Y, iw.Width, iw.Height);
                    if (rectA.IntersectsWith(rectB))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void TrayMenuUpdateShowMenu(object sender, DependencyPropertyChangedEventArgs e)
        {
            var isVisible = (bool)e.NewValue;
            if (!isVisible)
            {
                return;
            }
            ShowMenu.Items.Clear();

            var showAll = new MenuItem();
            showAll.Header = "All";
            showAll.Click += TrayMenuShowAll_OnClick;
            ShowMenu.Items.Add(showAll);

            foreach (IconWindow iw in Containers.Values)
            {
                var klotureMenuItem = new MenuItem();
                klotureMenuItem.Header = iw.ContainerName;
                klotureMenuItem.Tag = iw.ID;
                klotureMenuItem.Click += TrayMenuShow_OnClick;
                ShowMenu.Items.Add(klotureMenuItem);
            }
        }

        private void TrayMenuGlobalFind_OnClick(object sender, RoutedEventArgs e)
        {
            IsGlobalFind = !IsGlobalFind;
        }


        public IconWindow GetIconWindow(string ID)
        {
            if (!String.IsNullOrEmpty(ID) && Containers.ContainsKey(ID))
            {
                return Containers[ID];
            }
            return null;
        }
        private void DisconnectOverflowTo(IconWindow parent)
        {
            IconWindow child = GetIconWindow(parent.OverflowChildID);
            if (child != null)
            {
                child.SetOverflowParent(null);
            }
            parent.SetOverflowChild(null, 0);
        }
        private void DisconnectOverflowFrom(IconWindow child)
        {
            IconWindow parent = GetIconWindow(child.OverflowParentID);
            if (parent != null)
            {
                parent.SetOverflowChild(null, 0);
            }
            child.SetOverflowParent(null);
        }

        private void LinkKlotures(IconWindow from, int limit, IconWindow to)
        {
            DisconnectOverflowFrom(to);
            DisconnectOverflowTo(from);

            from.SetOverflowChild(to, limit);
            to.SetOverflowParent(from);
        }


        internal void DisplayKlotureSettings(IconWindow iconWindow)
        {
            KlotureMetadata settings = iconWindow.ToMetadata();
            KlotureSettingsDialog kdg = new KlotureSettingsDialog(Containers.Values.ToArray(), settings);
            kdg.SettingsChanged += KlotureSettingsDialog_SettingsChanged;
            kdg.Show();
        }

        private void KlotureSettingsDialog_SettingsChanged(object sender, KlotureMetadata e)
        {
            IconWindow iw = GetIconWindow(e.ID);
            if (iw != null)
            {
                KlotureMetadata origSettings = iw.ToMetadata();
                if (origSettings.OverflowChildID != e.OverflowChildID)
                {
                    DisconnectOverflowTo(iw);
                }
                if (origSettings.OverflowParentID != e.OverflowParentID)
                {
                    DisconnectOverflowFrom(iw);
                }
                iw.ApplyMetadata(e);
                IconWindow child = GetIconWindow(iw.OverflowChildID);
                if (child != null)
                {
                    LinkKlotures(iw, e.OverflowLimit, child);
                }
            }

            SaveSettings();
        }
    }
}
