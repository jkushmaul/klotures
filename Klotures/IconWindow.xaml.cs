﻿using Klotures.lib.jkushmaul;
using Klotures.lib.jkushmaul.controls;
using Klotures.lib.jkushmaul.controls.draggable;
using Klotures.lib.jkushmaul.controls.resizable;
using Klotures.lib.SharpShellHidden;
using Klotures.settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Klotures
{
    /// <summary>
    /// Interaction logic for IconWindowControl.xaml
    /// </summary>
    public partial class IconWindow : UserControl, IDraggable, IResizable
    {
        public event EventHandler SettingsChanged;
        public event EventHandler FilterEnabled;
        public event EventHandler FilterChanged;
        public event EventHandler FilterCancel;

        private ResizableBorder ResizeBorder;

        public string OverflowParentID { get; set; }
        public string OverflowChildID { get; set; }
        public int OverflowLimit { get; set; }


        public string ID { get;  }
        public bool IsMinimized { get; protected set; }
        public double OriginalHeight { get; set; }

        public bool IsFinding => Finder.Visibility == Visibility.Visible;
        public bool IsEditing => EditBlock.EditBlock.Visibility == Visibility.Visible;
        public bool IsLocked { get { return LockButton.IsLocked; } }
        public string ContainerName { get { return EditBlock.Text; } set { EditBlock.Text = value; } }

        public bool IsDeleted { get; private set; }
        private bool _isDesktopWindow = false;
        public bool IsDesktopWindow
        {
            get
            {
                return _isDesktopWindow;
            }
            set
            {
                _isDesktopWindow = value;
                deleteMenuItem.Visibility = _isDesktopWindow ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        public Point Position
        {
            get
            {
                return new Point(Canvas.GetLeft(this), Canvas.GetTop(this));
            }

            set
            {
                Canvas.SetLeft(this, value.X);
                Canvas.SetTop(this, value.Y);
            }
        }

        Border IResizable.ResizeBorder => this.Border;
        public Control ResizableControl => this;

        private Brush OriginalBackgroundBrush;
        private DragDropHelper draggableWindow;

        private MenuItem sortMenuItem = null;
        private MenuItem deleteMenuItem = null;

        private MainIconWindow MainIconWindow = null;

        private IconWindow(MainIconWindow mainIconWindow, KlotureMetadata metadata) : this(metadata.ID, mainIconWindow)
        {
            initMetadata = metadata;
            IsDesktopWindow = metadata.IsDefault;
            Position = new Point(metadata.Left, metadata.Top);
            Width = metadata.Width;
            OriginalHeight = metadata.Height;
            Height = metadata.Height;
            ApplyMetadata(metadata);
        }
        public void ApplyMetadata(KlotureMetadata metadata)
        {
            OverflowParentID = metadata.OverflowParentID;
            OverflowChildID = metadata.OverflowChildID;
            OverflowLimit = metadata.OverflowLimit;
            ContainerName = metadata.Name;
            LockButton.IsLocked = metadata.IsLocked;
            SetSortMethod(metadata.SortMethod);
            SetVisible(metadata.IsVisible);

            SetMinimized(metadata.IsMinimized, false);
        }

        public IconWindow(MainIconWindow mainIconWindow) : this(Guid.NewGuid().ToString(), mainIconWindow)
        {

        }

        public IconWindow(string ID, MainIconWindow mainIconWindow)  
        {
            InitializeComponent();

            this.ID = String.IsNullOrEmpty(ID) ? Guid.NewGuid().ToString() : ID;
 
            this.MainIconWindow = mainIconWindow;
            IconsLv.IconWindowParent = this;

            foreach (MenuItem m in EditBlock.ContextMenu.Items)
            {
                switch (m.Tag)
                {
                    case "deleteMenuItem":
                        deleteMenuItem = m;
                        break;
                    case "sortMenuItem":
                        sortMenuItem = m;
                        break;
                }
            }

            OriginalBackgroundBrush = Background;
            if (double.IsNaN(Height))
            {
                Height = 100;
            }
            this.OriginalHeight = Height;

            ResizeBorder = new ResizableBorder(this);
            draggableWindow = new DragDropHelper(this, EditBlock);
            draggableWindow.OnDragEvent += DraggableWindow_OnDragEvent;
        }

        public void FireSettingsChanged()
        {
            if(SettingsChanged != null)
            {
                SettingsChanged(this, EventArgs.Empty);
            }
        }

        #region "Serialization"
        internal KlotureMetadata ToMetadata()
        {
            KlotureMetadata m = new KlotureMetadata();
            m.ID = this.ID;
            m.Width = this.Width;
            m.Height = this.Height;
            Point p = this.Position;
            m.Left = p.X;
            m.Top = p.Y;
            m.Name = this.ContainerName;
            m.IsDefault = this.IsDesktopWindow;
            m.IsLocked = LockButton.IsLocked;
            m.IsVisible = Visibility == Visibility.Visible;
            m.IsMinimized = IsMinimized;
            m.Height = OriginalHeight;
            m.SortMethod = IconsLv.SortMethod;
            m.OverflowChildID = OverflowChildID;
            m.OverflowParentID = OverflowParentID;
            m.OverflowLimit = OverflowLimit;

            // If this is an overflow, we do not store it's parent's items, it's already storing them.
            if (String.IsNullOrEmpty(OverflowParentID))
            {
                foreach (IconControl ic in IconsLv.SourceCollection)
                {
                    m.ShortCuts.Add(new IconMetadata(ic.Path, ic.ActivationCount));
                }
            }

            return m;
        }

        internal void RefreshView()
        {
            IconsLv.RefreshView();
        }

        public static IconWindow FromMetadata(MainIconWindow mainIconWindow, KlotureMetadata metadata)
        {
            IconWindow iw = new IconWindow(mainIconWindow, metadata);

            Application.Current.Dispatcher.BeginInvoke(
                    DispatcherPriority.Loaded,
                    new Action(() => iw.SetMinimized(metadata.IsMinimized, true)));
     
            return iw;
        }

        private KlotureMetadata initMetadata = null;
        internal void TakeShellItems(ref List<ShellItem> shellItems)
        {
            List<IconControl> icList = new List<IconControl>();
            List<DispatcherOperation> opList = new List<DispatcherOperation>();


            var iterator = new List<ShellItem>(shellItems);

            foreach (var s in iterator)
            {
                bool take = true;
                int ActivationCount = 0;

                if (initMetadata != null)
                {
                    take = false;
                    if (initMetadata.ShortCuts != null)
                    {
                        var shortcutMeta = initMetadata.ShortCuts.Find(i => i.Path == s.Path);
                        if (shortcutMeta != null)
                        {
                            ActivationCount = shortcutMeta.ActivationCount;
                            take = true;
                        }
                    }
                    else if (initMetadata.Paths != null && initMetadata.Paths.Contains(s.Path))
                    {
                        take = true;
                    }
                }
                if (take)
                {
                    shellItems.Remove(s);
                    opList.Add(Application.Current.Dispatcher.BeginInvoke(
                          DispatcherPriority.Background,
                          new Action(() =>
                          {
                              IconControl ic = new IconControl(this, s, ActivationCount);
                              icList.Add(ic);
                          })
                      ));
                }
            }
            initMetadata = null;
            opList.ForEach(a => a.Wait());
            Application.Current.Dispatcher.Invoke(() => IconsLv.AddShortcutRange(icList));
        }
        #endregion
        

        #region "Shortcuts"
        internal void TakeShortcut(IconControl ic)
        {
            ic.ParentIconContainer.RemoveShortcut(ic);
            AddShortcut(ic, true);
            FireSettingsChanged();
        }
        internal void RenameShortcut(IconControl ic, string path2)
        {
            ShellItem shellItem = ShellItem.DesktopShellFolder.GetChildren(ShellItem.ChildTypes.Files | ShellItem.ChildTypes.Folders).FirstOrDefault(i => i.Path == path2);
            if (shellItem != null)
            {
                IconsLv.RemoveShortcut(ic);
                ic.UpdateShellItem(shellItem);
                IconsLv.AddShortcut(ic);
            }
        }

        internal void AddShortcut(ShellItem shellItem, bool saveSettings)
        {
            IconControl ic = new IconControl(this, shellItem);
            AddShortcut(ic, saveSettings);
        }
        internal void AddShortcut(IconControl ic, bool saveSettings)
        {
            if (String.IsNullOrEmpty(ic.Path))
            {
                return;
            }
            ic.ParentIconContainer = this;

            IconsLv.AddShortcut(ic);
            if (saveSettings)
            {
                FireSettingsChanged();
            }
        }

        internal void RemoveShortcut(IconControl ic)
        {
            IconsLv.RemoveShortcut(ic);
            ic.ParentIconContainer = null;
            FireSettingsChanged();
        }


        internal bool HasShortcut(string path)
        {
            return IconsLv.GetShortcut(path) != null;
        }

        public virtual IconControl GetShortcut(string path)
        {
            return IconsLv.GetShortcut(path);
        }
        #endregion

        #region "Minimize"
        public void SetMinimized(bool minimize, bool temporary)
        {

            if (!temporary)
            {
                IsMinimized = minimize;
            }

            if (Finder.Visibility == Visibility.Collapsed && IsMinimized)
            {
                SetFindMode(false, true);
            }
            if (minimize)
            {
                ContentPanel.Visibility = Visibility.Collapsed;
                Height = TitleBar.Height;
                ResizeBorder.IsEnabled = false;
            }
            else if (!minimize)
            {
                Height = OriginalHeight;
                ContentPanel.Visibility = Visibility.Visible;
                if (!IsLocked)
                {
                    ResizeBorder.IsEnabled = true;
                }
            }
            if (!temporary)
            {
                FireSettingsChanged();
            }
        }

        private void EditBlock_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (IsLocked)
            {
                return;
            }

            if (e.ClickCount == 2)
            {
                SetMinimized(!IsMinimized, false);
                e.Handled = true;
            }
        }
        private void BaseIconWindow_MouseEnter(object sender, MouseEventArgs e)
        {
            Focus();
            if (IsMinimized)
            {
                SetMinimized(false, true);
            }
        }

        private void BaseIconWindow_MouseLeave(object sender, MouseEventArgs e)
        {
            if (IsMinimized)
            {
                SetMinimized(true, true);
            }
        }
        #endregion

        #region "FindControl"
        private void Finder_FindEventHandler(object sender, FindControl.FindEventArgs e)
        {
            string filter = e.SearchKey.ToLower();
            if (FilterChanged != null)
            {
                StringEventArgs se = new StringEventArgs(filter);
                FilterChanged(this, se);
            }
            else
            {
                SetFilter(filter);
            }

        }

        public void SetFilter(string filter)
        {
            IconsLv.SetFilter(filter);
        }

        private void EnableFindShortcut(object sender, ExecutedRoutedEventArgs e)
        {
            SetFindMode(true, true);
            if (FilterEnabled != null)
            {
                FilterEnabled(this, EventArgs.Empty);
            }
        }
        private void Finder_CancelFindEventHandler(object sender, EventArgs e)
        {
            SetFindMode(false, true);
            if (FilterEnabled != null)
            {
                FilterCancel(this, EventArgs.Empty);
            }
        }



        public void SetFindMode(bool isFind, bool isPrimary)
        {
            if (IsEditing)
            {
                return;
            }
            if (isFind)
            {
                if (isPrimary)
                {
                    this.Finder.Visibility = Visibility.Visible;
                }
                SetMinimized(false, true);
            }
            else
            {
                this.Finder.Visibility = Visibility.Collapsed;
            }
        }
        #endregion

        #region "EditControl"
        private void SetEditMode(bool isEdit)
        {
            if (IsFinding)
            {
                return;
            }
            //disable drag drop
            draggableWindow.Enabled = !isEdit;
            EditBlock.SetEditMode(isEdit);
        }

        private void Rename_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            SetEditMode(true);
        }
     
        private void EditBlock_OnEditSave(object sender, EditControl.EditEventArgs e)
        {
            string new_name = e.Text;
            if (string.IsNullOrWhiteSpace(new_name))
            {
                MessageBox.Show("Name must not be empty");
                return;
            }
            if (new_name == ContainerName)
            {
                return;
            }

            ContainerName = new_name;
            FireSettingsChanged();
        }
        #endregion

        #region "move event handlers"

        private void DraggableWindow_OnDragEvent(object sender, DraggableEventArgs e)
        {
            Point p = e.NewPosition;
            //This is the only reason MainIconWindow is backreferenced like this.   There's got to be a better way...
            bool collides = MainIconWindow.WillCauseWindowOverlap(this, p, new Size(this.Width, this.Height));

       

            if (e.state == DraggableEventArgs.CompletionState.COMPLETE || e.state == DraggableEventArgs.CompletionState.CANCELED)
            {
                CancelDrag(collides || e.state == DraggableEventArgs.CompletionState.CANCELED);
            } else
            {
                this.Opacity = 0.5;
                this.Background = !collides ? OriginalBackgroundBrush : Brushes.Red;
                this.Position = p;
            }
        }

        private void CancelDrag(bool abort)
        {
            this.Background = OriginalBackgroundBrush;
            this.Opacity = 1;

            //abort = abort || MainWindow.WillCauseWindowOverlap(this, this.Position, new Size(this.Width, this.Height));
            if (abort)
            {
                this.Position = draggableWindow.OriginalLocation;
            }
            draggableWindow.StopMove();
            FireSettingsChanged();
        }


         
        private void EditBlock_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (draggableWindow.State == DragState.NO_DRAG)
            {
                if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.F)
                {
                    e.Handled = true;
                    EnableFindShortcut(null, null);
                }
                return;
            }

            if (e.Key == Key.Escape)
            {
                e.Handled = true;
                CancelDrag(true);
                return;
            }
        }
        #endregion

        private void SetLocked(bool isLocked)
        {
            if (isLocked)
            {
                EditBlock.Cursor = Cursors.No;
                ResizeBorder.IsEnabled = false;
            }
            else
            {
                EditBlock.Cursor = Cursors.Arrow;
                if (!IsMinimized)
                {
                    ResizeBorder.IsEnabled = true;
                }
            }

            EditBlock.IsEnabled = !isLocked;
            draggableWindow.Enabled = !isLocked;

        }
        private void LockControl_LockEventHandler(object sender, LockControl.LockEventArgs e)
        {
            SetLocked(e.IsLocked);
            FireSettingsChanged();
        }

        public void SetVisible(bool visible)
        {
            if (visible)
            {
                this.Visibility = Visibility.Visible;
            }
            else
            {
                this.Visibility = Visibility.Hidden;
            }
            FireSettingsChanged();
        }

        public void SetOverflowChild(IconWindow overflow, int limit)
        {
            IconListViewControl lv = null;
            if (overflow != null)
            {
                lv = overflow.IconsLv;
                OverflowChildID = overflow.ID;
                OverflowLimit = limit;
            } else
            {
                OverflowLimit = 0;
                OverflowChildID = "";
            }
            
            IconsLv.SetOverflow(lv, OverflowLimit);
            FireSettingsChanged();
        }
        public void SetOverflowParent(IconWindow parent)
        {
            OverflowLimit = 0;

            if (parent != null)
            {
                OverflowParentID = parent.ID;
            }
            else
            {
                SetOverflowChild(null, 0);
            }
            
            FireSettingsChanged();
        }

        private void Scroller_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scv = this.Scroller;
            scv.ScrollToVerticalOffset(scv.VerticalOffset - e.Delta);
            e.Handled = true;
        }

        private void Icons_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            Scroller_PreviewMouseWheel(sender, e);
        }


        private void EditSettings_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MainIconWindow.DisplayKlotureSettings(this);
          
        }
 

        private void Delete_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (IsDesktopWindow)
            {
                return;
            }


            MessageBoxResult result = System.Windows.MessageBox.Show(
                "Are you sure you want to remove the container?", "Confirm delete", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.OK)
            {
                this.Visibility = Visibility.Hidden;
                IsDeleted = true;
                FireSettingsChanged();
            }
        }
        public void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (!IsMinimized && OriginalHeight != ActualHeight)
            {
                OriginalHeight = ActualHeight;
                FireSettingsChanged();
            }
        }
        private void BaseIconWindow_Loaded(object sender, RoutedEventArgs e)
        {
            //don't bother saving properties due to init resize.
            SizeChanged += this.Window_SizeChanged;
        }

        private void Hide_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            SetVisible(false);
        }


        private void SetSortMethod(SortMethod newMethod)
        {
            IconsLv.SetSortMethod(newMethod);
            foreach (MenuItem m in sortMenuItem.Items)
            {
                SortMethod mSort = SortMethod.None;
                switch (m.Tag)
                {
                    case "sortByNone":
                        mSort = SortMethod.None;
                        break;
                    case "sortByAlpha":
                        mSort = SortMethod.Alphabetical;
                        break;
                    case "sortByUse":
                        mSort = SortMethod.Frequency;
                        break;
                }
                m.IsChecked = mSort == IconsLv.SortMethod;
            }
        }



        private void sortBy_MenuItem_Click(object sender, RoutedEventArgs e)
        {

            MenuItem m = (MenuItem)sender;
            SortMethod newMethod = IconsLv.SortMethod;
            switch (m.Tag)
            {
                case "sortByNone":
                    newMethod = SortMethod.None;
                    break;
                case "sortByAlpha":
                    newMethod = SortMethod.Alphabetical;
                    break;
                case "sortByUse":
                    newMethod = SortMethod.Frequency;
                    break;
            }

            SetSortMethod(newMethod);
            FireSettingsChanged();
        }

        private void UserControl_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Control && Keyboard.IsKeyDown(Key.F)) 
            {
                EnableFindShortcut(null, null);
            }
        }

    

    }
}