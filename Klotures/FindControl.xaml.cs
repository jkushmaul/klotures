﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Klotures
{
    /// <summary>
    /// Interaction logic for FindControl.xaml
    /// </summary>
    public partial class FindControl : UserControl
    {
        public class FindEventArgs
        { 
            public string SearchKey { get; protected set; }
            public FindEventArgs(string key)
            {
                this.SearchKey = key;
            }
        }

        public event EventHandler<FindEventArgs> FindEventHandler;
        public event EventHandler CancelFindEventHandler;

        public FindControl()
        {
            InitializeComponent();
            this.ItemSearch.FontSize *= 2;
            FindEventHandler = null;
        }

        private void ItemSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (FindEventHandler != null)
            {
                FindEventHandler(this, new FindEventArgs(ItemSearch.Text));
            }
        }

        private void ItemSearch_LostFocus(object sender, RoutedEventArgs e)
        {
            //leaving this here, empty -
            //this is actually useful to not be handled, so to not reset on lost focus - 
            //you can't double click on something and not lose focus
            //you can't create more than one filter at a time.
        }


        private void SendCancelFindEvent()
        {
            ItemSearch.Text = "";
            if (CancelFindEventHandler != null)
            {
                CancelFindEventHandler(this, EventArgs.Empty);
            }
        }

        private void ItemSearch_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape || e.Key == Key.Enter)
            {
                SendCancelFindEvent();
            }
        }

 

        private void FindControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (Visibility == Visibility.Visible)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    ItemSearch.Focus();
                }), DispatcherPriority.Render);
            } else
            {
                ItemSearch.Text = "";
            }
        }
    }
}
