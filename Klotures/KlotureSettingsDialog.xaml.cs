﻿using Klotures.lib.jkushmaul;
using Klotures.settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Klotures
{
    /// <summary>
    /// Interaction logic for KlotureSettingsDialog.xaml
    /// </summary>
    public partial class KlotureSettingsDialog : Window
    {
        private bool IsClosing = false;

        public event EventHandler<KlotureMetadata> SettingsChanged;

        private String forID;
        public KlotureSettingsDialog() : this(null, null)
        {
            
        }

        public KlotureSettingsDialog(IconWindow[] iconWindows, KlotureMetadata settingsData)
        {
            InitializeComponent();
            if (settingsData == null || iconWindows == null)
            {
                return;
            }
            forID = settingsData.ID;

            foreach (string method in Enum.GetNames(typeof(SortMethod)))
            {
                Setting_SortMethod.Items.Add(method);
            }

            Setting_Name.Text = settingsData.Name;

            ComboBoxItem ci = new ComboBoxItem();
            ci.Content = "<Disconnect>";
            ci.Tag = "";
            Setting_OverflowTo.Items.Add(ci);

            foreach (IconWindow iw in iconWindows)
            {
                //do not display this node as a choice for itself.
                if (iw.ID == settingsData.ID)
                {
                    continue;
                }
                //do not disiplay nodes already assigned a parent
                if (!String.IsNullOrEmpty(iw.OverflowParentID) && iw.OverflowParentID != settingsData.ID)
                {
                    continue;
                }
                //Klotures that already have children - that's Ok, we can chain overflows.

                ci = new ComboBoxItem();
                ci.Content = iw.ContainerName;
                ci.Tag = iw.ID;
                Setting_OverflowTo.Items.Add(ci);

                if (!String.IsNullOrEmpty(settingsData.OverflowChildID) && iw.ID == settingsData.OverflowChildID)
                {
                    Setting_OverflowTo.SelectedValue = ci;
                }
                if (!String.IsNullOrEmpty(settingsData.OverflowParentID) && iw.ID == settingsData.OverflowParentID)
                {
                    Setting_OverflowFrom.Content = iw.ContainerName + " (Disconnect)";
                    Setting_OverflowFrom.Tag = iw.ID;
                }
            }
            if (String.IsNullOrEmpty(settingsData.OverflowParentID))
            {
                Setting_OverflowFrom_Click(null, null);
            }

            Setting_IsVisible.IsChecked = settingsData.IsVisible;
            Setting_IsMinimized.IsChecked = settingsData.IsMinimized;
            Setting_IsLocked.IsChecked = settingsData.IsLocked;
            Setting_SortMethod.SelectedItem = settingsData.SortMethod.ToString();

            Setting_OverflowLimit.Text = settingsData.OverflowLimit.ToString();
        }

        private bool OptionalBoolean(CheckBox val)
        {
            return val.IsChecked.HasValue && val.IsChecked.Value;
        }
        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            KlotureMetadata settingsData = new KlotureMetadata();
            settingsData.ID = forID;

            SortMethod sortMethod;
            if (!Enum.TryParse(Setting_SortMethod.SelectedValue as string, out sortMethod))
            {
                MessageBox.Show("There was an error saving, SortMethod invalid");
                return;
            }

            ComboBoxItem ci = Setting_OverflowTo.SelectedValue as ComboBoxItem;

            settingsData.IsMinimized = OptionalBoolean(Setting_IsMinimized);
            settingsData.IsLocked = OptionalBoolean(Setting_IsLocked);
            settingsData.IsVisible = OptionalBoolean(Setting_IsVisible);
            settingsData.SortMethod = sortMethod;
            settingsData.OverflowChildID = ci == null ? "" : ci.Tag as string;
            settingsData.OverflowLimit = String.IsNullOrEmpty(settingsData.OverflowChildID) ? 0 : int.Parse(Setting_OverflowLimit.Text);
            settingsData.OverflowParentID = Setting_OverflowFrom.IsEnabled ? Setting_OverflowFrom.Tag as string : "";
            settingsData.Name = Setting_Name.Text;


            SettingsChanged(this, settingsData);
            IsClosing = true;
            Close();
        }
 
        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            IsClosing = true;
            Close();
        }

        private void Setting_OverflowTo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string selectedId = "";
            ComboBoxItem ci = Setting_OverflowTo.SelectedItem as ComboBoxItem;
            if (ci != null)
            {
                selectedId = ci.Tag as string;
            }
            if (String.IsNullOrEmpty(selectedId))
            {
                // disable limit
                Setting_OverflowLimit.IsEnabled = false;
                Row_OverflowLimit.Height = new GridLength(0);
            } else
            {
                Setting_OverflowLimit.IsEnabled = true;
                Row_OverflowLimit.Height = new GridLength(0, GridUnitType.Auto);
            }
            
        }
 

        private void Setting_OverflowFrom_Click(object sender, RoutedEventArgs e)
        {
            Setting_OverflowFrom.Content = "<Disconnected>";
            Setting_OverflowFrom.IsEnabled = false;
            Setting_OverflowFrom.Visibility = Visibility.Collapsed;
            Row_OverflowFrom.Height = new GridLength(0);
        }
    }
}
