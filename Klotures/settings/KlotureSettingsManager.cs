﻿using Klotures.lib.SharpShellHidden;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Controls;

namespace Klotures.settings
{
    internal class KlotureSettingsManager : IKloturesMetadataProvider
    {
        private static readonly object SyncLock = new object();
        private IKloturesMetadataProvider metadataProvider;


        public KlotureMetadata[] KlotureMetaDatas => Klotures.ItemsArray;

        internal KlotureSettingsManager(IKloturesMetadataProvider metadataProvider)
        {
            this.metadataProvider = metadataProvider;

            //First time users have a hard time being first
            if (this.Klotures == null)
            {
                this.Klotures = new KlotureCollection();
            }
        }
        private void Commit()
        {
            Properties.Settings.Default.Save();
        }
        private KlotureCollection Klotures
        {
            get
            {
                return Properties.Settings.Default.Klotures;
            }
            set
            {
                Properties.Settings.Default.Klotures = value;
            }
        }

        internal bool FirstStartup
        {
            get
            {
                return Properties.Settings.Default.FirstStartup;
            }
            set
            {
                Properties.Settings.Default.FirstStartup = value;
            }
        }
        internal bool GlobalFind
        {
            get
            {
                return Properties.Settings.Default.GlobalFind;
            }
            set
            {
                Properties.Settings.Default.GlobalFind = value;
            }
        }


        internal void Lock(Action action)
        {
            lock (SyncLock)
            {
                action();
            }
        }

        internal void Save(Action action)
        {
            Lock(() =>
            {
                action();
                KlotureCollection newKlotures = new KlotureCollection();
                //this will include default.
                foreach (KlotureMetadata m in metadataProvider.KlotureMetaDatas)
                {
                    newKlotures.Items.Add(m.Name, m);
                }
                Klotures = newKlotures;
                Commit();
            });
        }
    }

}

