﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Xml.Serialization;
using System.Linq;
using System.Windows;

namespace Klotures.settings
{
    [Serializable]
    [SettingsSerializeAs(System.Configuration.SettingsSerializeAs.Xml)]
    public class KlotureCollection
    {
        [XmlIgnore]
        public Dictionary<string, KlotureMetadata> Items { get; private set; } = new Dictionary<string, KlotureMetadata>();

        [XmlElement]
        public KlotureMetadata DefaultDesktopKloture
        {
            get;
            set;
        }


        [XmlArray]
        public KlotureMetadata[] ItemsArray
        {
            get
            {
                return Items.Values.ToList().ToArray();
            }
            set
            {
                Items = value.ToDictionary(i => i.Name);
            }
        }
    }
}
