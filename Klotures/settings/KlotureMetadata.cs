﻿using Klotures.lib.jkushmaul;
using Klotures.lib.SharpShellHidden;
using SharpShell.Interop;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;

namespace Klotures.settings
{
    [Serializable]
    [SettingsSerializeAs(SettingsSerializeAs.Xml)]
    public class IconMetadata
    {

        [XmlElement("Path")]
        public string Path { get; set; }

        [XmlAttribute]
        public int ActivationCount { get; set; }

        public IconMetadata()
        {

        }
        public IconMetadata(string Path, int ActivationCount)
        {
            this.Path = Path;
            this.ActivationCount = ActivationCount;
        }
    }


    [Serializable]
    [SettingsSerializeAs(SettingsSerializeAs.Xml)]
    public class KlotureMetadata
    {
        public KlotureMetadata()
        {
            Paths = new List<string>();
            ShortCuts = new List<IconMetadata>();
        }

        [XmlAttribute]
        public string OverflowParentID { get; set; }

        [XmlAttribute]
        public string OverflowChildID { get; set; }

        [XmlAttribute]
        public int OverflowLimit { get; set; }

        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public string ID { get; set; }
        [XmlAttribute]
        public double Top { get; set; }
        [XmlAttribute]
        public double Left { get; set; }
        [XmlAttribute]
        public double Width { get; set; }
        [XmlAttribute]
        public double Height { get; set; }

        [XmlAttribute]
        public bool IsDefault { get; set; }
        [XmlAttribute]
        public bool IsLocked { get; set; }
        [XmlAttribute]
        public bool IsVisible { get; set; }
        [XmlAttribute]
        public bool IsMinimized{ get; set; }
        [XmlAttribute]
        public SortMethod SortMethod { get; set; }

        [XmlElement("Paths")]
        public List<string> Paths { get; set; }
        [XmlElement("ShortCuts")]
        public List<IconMetadata> ShortCuts{ get; set; }

    }


}
