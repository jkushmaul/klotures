﻿using Klotures.lib.jkushmaul;
using Klotures.lib.jkushmaul.controls;
using Klotures.lib.jkushmaul.controls.draggable;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input; 

namespace Klotures
{
    /// <summary>
    /// Interaction logic for IconListViewControl.xaml
    /// </summary>
    public partial class IconListViewControl : UserControl, IDraggable
    {
        public IconWindow IconWindowParent { get; set; }

        private string Filter;
        Point origin = new Point(0, 0);
        public Point Position => origin;

        public IEnumerable<IconControl> SourceCollection { get { return _Items; } }

        public SortMethod SortMethod { get; internal set; }

        DragDropHelper dragHelper;


        private IconListViewControl overflowTo;
        private SortedObservableCollection<IconControl, IconControlComparer> _items;
        private OverflowListCollectionView<IconControl, IconControlComparer> _view;

        SortedObservableCollection<IconControl, IconControlComparer> _Items
        {
            get => _items;
            set
            {
                _items = value;
                _View = new OverflowListCollectionView<IconControl, IconControlComparer>(_items);
            }
        }

        private OverflowListCollectionView<IconControl, IconControlComparer>  _View
        {
            get => _view;
            set
            {
                _view = value;
                _view.OverflowNotifier += PushOverflow;
                _Icons.ItemsSource = _view;
            }
        }
        internal IconListViewControl OverflowTo 
        {
            get => overflowTo;
            set
            {
                overflowTo = value;
                PushOverflow(null, null);
            }
        }

        public void SetOverflow(IconListViewControl overflowTo, int limit)
        {
            _View.Limit = limit;
            OverflowTo = overflowTo;
        }

        private void PushOverflow(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (overflowTo == null || _View.Overflow == null)
            {
                return;
            }
            List<IconControl> list = new List<IconControl>();
            foreach (IconControl ic in _View.Overflow)
            {
                list.Add(ic);
            }
            overflowTo._Items = new SortedObservableCollection<IconControl, IconControlComparer>(list);
        }

        internal void SetSortMethod(SortMethod sortMethod)
        {
            this.SortMethod = sortMethod;
            _View.CustomSort = new IconControlComparer(SortMethod);
        }

        public IconListViewControl()
        {
     
            InitializeComponent();

            _Items = new SortedObservableCollection<IconControl, IconControlComparer>();

            IconWindowParent = null;
            dragHelper = new DragDropHelper(this, _Icons);
            dragHelper.OnDragInitData += DragHelper_OnDragInitData;
        }
     
        void CancelDrag()
        {
            dragHelper.StopMove();
        }

        private void Icons_Drop(object sender, DragEventArgs e)
        {
            IconListViewControl iconLvc = e.Data.GetData(typeof(IconListViewControl)) as IconListViewControl;
            if (iconLvc != null && iconLvc != this)
            {
                if (e.Data.GetDataPresent(typeof(IconControl[])))
                {
                    IconControl[] icArr = (IconControl[])e.Data.GetData(typeof(IconControl[]));
                    if (icArr != null)
                    {
                        foreach (IconControl ic in icArr)
                        {
                            IconWindowParent.TakeShortcut(ic);
                        }

                    }
                }
            }
            e.Handled = true;
            if (iconLvc != null)
            {
                iconLvc.CancelDrag();
            }
        }

        protected void Icons_DragOver(object sender, DragEventArgs e)
        {
            e.Handled = true;
            e.Effects = DragDropEffects.None;

            IconListViewControl iconLvc = e.Data.GetData(typeof(IconListViewControl)) as IconListViewControl;
            if (iconLvc != null && iconLvc != this)
            {
                IconControl[] icArr = (IconControl[])e.Data.GetData(typeof(IconControl[]));
                if (icArr != null)
                {
                    e.Effects = DragDropEffects.Move;
                }
            }
        }

  

        protected void Icons_DragEnter(object sender, DragEventArgs ea)
        {
            Icons_DragOver(sender, ea);
        }

        private void DragHelper_OnDragInitData(object sender, System.EventArgs e)
        {
            var dragData = new DataObject();
            List<IconControl> selectedItems = new List<IconControl>();
            foreach (IconControl ic in _Icons.SelectedItems)
            {
                if (ic != null)
                {
                    selectedItems.Add(ic);
                }
            }

            dragData.SetData(typeof(IconControl[]), selectedItems.ToArray());
            dragData.SetData(typeof(IconListViewControl), this);
            DragDrop.DoDragDrop(this, dragData, DragDropEffects.Move);
        }



        private void Icons_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            CancelDrag();
            var item = sender as ListViewItem;

            if (item != null)
            {
                List<Key> keys = new List<Key>() { Key.LeftCtrl, Key.RightCtrl, Key.LeftShift, Key.RightShift };
                if (keys.FindAll(k => Keyboard.IsKeyDown(k)).Count == 0)
                {
                    bool single = _Icons.SelectedItems.Count < 2;
                    if (item.IsSelected)
                    {
                        if (!single)
                        {
                            _Icons.UnselectAll();
                            item.IsSelected = true;
                            e.Handled = true;
                        } 
                    }
                }
            }
        }

        internal void RefreshView()
        {
            SetSortMethod(SortMethod);
        }

        /// <summary>
        /// Prevent ListView from unselecting the item as you click to drag.
        /// Side effect is that you cannot unselect an item by clicking it which mimics desktop anyways.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Icons_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null && (dragHelper.State != DragState.NO_DRAG || item.IsSelected && _Icons.SelectedItems.Count > 1))
            {
                e.Handled = true;
            }
        }
         
        /// <summary>
        /// I can't catch the mouse with captureMouse for some reason off window, so I had to do this.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Icons_MouseEnter(object sender, MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
            {
                CancelDrag();
            }
        }

     

   

        public class IconControlComparer : IComparer, IComparer<IconControl>
        { 
            private readonly SortMethod _sortMethod;

            public int Compare(object x, object y)
            {
                IconControl ic1 = x as IconControl;
                IconControl ic2 = y as IconControl;

                return Compare(ic1, ic2);

               
            }

            public int Compare(IconControl ic1, IconControl ic2)
            {
                switch (_sortMethod)
                {
                    case SortMethod.Alphabetical:
                        return ic1.DisplayName.CompareTo(ic2.DisplayName);
                    case SortMethod.Frequency:
                        return ic2.ActivationCount - ic1.ActivationCount;
                    case SortMethod.None:
                        return ic1.Rank - ic2.Rank;
                }
                return 0;
            }

            public IconControlComparer(SortMethod sortMethod)
            { 
                _sortMethod = sortMethod;
            }
        }

        internal void AddShortcutRange(List<IconControl> icList)
        {
            foreach(IconControl ic in icList)
            {
                AddShortcut(ic);
            }
        }

        internal void AddShortcut(IconControl ic)
        {
            ic.Margin = new Thickness(5, 5, 5, 5);
            _Items.Add(ic);
        }

        internal void RemoveShortcut(IconControl ic)
        {
            _Items.Remove(ic);
        }

        internal void SetFilter(string filter)
        {
            Filter = filter.ToLower();
            _Icons.Items.Filter = ApplyFilter;
        }

        private bool ApplyFilter(object obj)
        {
            IconControl ic = obj as IconControl;
            if (Filter.Length > 0 && ic != null)
            {
                return ic.DisplayName.ToLower().Contains(Filter);
            }
            return true;
        }

        internal IconControl GetShortcut(string path)
        {
            return _Items.FirstOrDefault(ic => ic.Path.ToLower().Equals(path, StringComparison.InvariantCultureIgnoreCase));
        }

    }
     
}
