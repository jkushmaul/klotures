﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klotures
{
    /// <summary>
    /// Interaction logic for LockButton.xaml
    /// </summary>
    public partial class LockControl : UserControl
    {
        public class LockEventArgs
        {
            public bool IsLocked { get; protected set; }
            public LockEventArgs(bool isLocked)
            {
                this.IsLocked = isLocked;
            }
        }

        public event EventHandler<LockEventArgs> LockEventHandler;
        

        public LockControl()
        {
            InitializeComponent();
            IsLocked = false;
            LockEventHandler = null;
        }
        private bool _IsLocked;
        public bool IsLocked
        {
            get
            {
                return _IsLocked;
            }
            set
            {
                _IsLocked = value;
                RefreshLockMode();
            }
        }
        protected void RefreshLockMode()
        {
            if (IsLocked)
            {
                UnlockButton.Visibility = Visibility.Collapsed;
                UnlockButton.IsEnabled = false;
                LockButton.Visibility = Visibility.Visible;
                LockButton.IsEnabled = true;
            }
            else
            {
                LockButton.Visibility = Visibility.Collapsed;
                LockButton.IsEnabled = false;
                UnlockButton.Visibility = Visibility.Visible;
                UnlockButton.IsEnabled = true;
            }
            if (LockEventHandler != null)
            {
                LockEventHandler(this, new LockEventArgs(IsLocked));
            }
        }
        private void LockButton_Click(object sender, MouseButtonEventArgs e)
        {
            IsLocked = false;
        }

        private void UnlockButton_Click(object sender, MouseButtonEventArgs e)
        {
            IsLocked = true;
        }
    }
}
